import getopt
import sys

from ExperimentResolver import ExperimentResolver
from electricNetwork.network import Network
from simulation.optimizer import NetworkOptimizer


class Application:
    @staticmethod
    def run(argv):
        number_of_replications = None
        max_simulation_time = None
        resistance_file = None
        reactance_file = None
        experiment_number = None
        try:
            opts, args = getopt.getopt(argv, "hl:m:r:x:e:",
                                       ["replications=", "max_time=", "resistance_file=",
                                        "reactance_file=", "experiment_number="])
        except getopt.GetoptError:
            Application.__argument_error()
            sys.exit(2)
        if opts.__len__() == 0:
            Application.__argument_error()
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                Application.__print_help(sys.stdout)
                sys.exit()
            elif opt in ("-l", "--replications"):
                number_of_replications = int(arg)
            elif opt in ("-m", "--max_time"):
                max_simulation_time = int(arg)
            elif opt in ("-r", "--resistance_file"):
                resistance_file = arg
            elif opt in ("-x", "--reactance_file"):
                reactance_file = arg
            elif opt in ("-e", "--experiment_number"):
                experiment_number = int(arg)

        Application.run_simulation(max_simulation_time, number_of_replications, resistance_file,
                                   reactance_file, experiment_number)

    @staticmethod
    def __print_help(output):
        print('main.py'
              '\n-l / --replications <number of replications> '
              '\n-m/--max_time <maximum simulation time> '
              '\n-r/--resistance_file <resistance file> '
              '\n-x/--reactance_file <reactance file>'
              '\n-e/--experiment_number experiment_number', file=output)

    @staticmethod
    def __argument_error():
        print('Error in arguments!', file=sys.stderr)
        Application.__print_help(sys.stderr)

    @staticmethod
    def run_simulation(max_simulation_time, number_of_replications, resistance_file, reactance_file,
                       experiment_number):
        network = Network(resistance_file, reactance_file, 1)
        network.init_electric_network()
        optimizer = NetworkOptimizer(network, 0.1)
        experiment_resolver = ExperimentResolver()
        experiment_resolver.prepare_scaling_experiments(network, optimizer, 1)
        simulation_core = experiment_resolver.resolve_scaling_experiment(experiment_number)
        simulation_core.run(number_of_replications, max_simulation_time)


if __name__ == '__main__':
    application = Application()
    application.run(sys.argv[1:])
