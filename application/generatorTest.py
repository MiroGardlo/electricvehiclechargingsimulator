from simulation.simulationUtils.generators.exponential_rng import ExponentialRNG

file = open("generator.txt", "w")
generator = ExponentialRNG(10)
for i in range(100000):
    file.writelines(str(generator.generate_sample()).__add__('\n'))
file.close()
