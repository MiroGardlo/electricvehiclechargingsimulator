from electricNetwork.network import Network
from simulation.experiments.simulationExperiments.willingnessToPayEffectWithStaticUsers import \
    WillingnessToPayEffectWithStaticUsersTest
from simulation.optimizer import NetworkOptimizer

network = Network("../networks/R4-linear-level-1.csv", "../networks/X4-linear-level-1.csv", 1)
network.init_electric_network()
optimizer = NetworkOptimizer(network, 0.1)
simulation_core = WillingnessToPayEffectWithStaticUsersTest(optimizer, 1, network)
simulation_core.run(1, 9999)
