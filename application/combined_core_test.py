from electricNetwork.network import Network
from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from simulation.experiments.simulationExperiments.realChargingExperiment import RealChargingExperiment
from simulation.experiments.simulationExperiments.realChargingScanning import ChargingScanningCore
from simulation.optimizer import NetworkOptimizer
from simulation.simulationCore.charging_simulation_logger import ChargingSimulationObserver
from simulation.simulationUtils.generators.EmpiricRNG import EmpiricRNG
from simulation.simulationUtils.generators.carTypeGenerator.car_type_generator import CarTypeGenerator
from simulation.simulationUtils.generators.deterministic_rng import DeterministicRNG

network = Network("../networks/R2.csv", "../networks/X2.csv", 1)
network.init_electric_network()
optimizer = NetworkOptimizer(network, 0.1)
observer = ChargingSimulationObserver("simulationLog.txt")
scanning_simulation_core = ChargingScanningCore(1, network, optimizer)
scanning_simulation_core.register_simulation_listener(observer)
strategy_generator = CarTypeGenerator(EmpiricRNG([DeterministicRNG(0), 1]), [lambda: StaticUserCar(1, 0, 10, 0)])
event_simulation_core = RealChargingExperiment(network, strategy_generator, 10)
event_simulation_core.register_simulation_listener(observer)
event_simulation_core.activity_scanning_simulation_core = scanning_simulation_core
scanning_simulation_core.event_simulation_core = event_simulation_core
event_simulation_core.run(2, 10)
