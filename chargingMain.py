import getopt
import os
import sys

from electricNetwork.network import Network
from simulation.experiments.simulationExperiments.experiment_with_random_arrivals import ExperimentWithRandomArrivals
from simulation.experiments.simulationExperiments.realChargingScanning import ChargingScanningCore
from simulation.optimizer import NetworkOptimizer
from simulation.simulationCore.charging_simulation_logger import ChargingSimulationObserver
from simulation.simulationCore.replication_simulation_logger import ReplicationSimulationObserver
from simulation.simulationUtils.generators.carTypeGenerator.car_type_generator import CarTypeGenerator
from simulation.simulationUtils.generators.carTypeGenerator.strategies_lambda_resolver import StrategiesLambdaResolver


class ChargingApplication:
    @staticmethod
    def run(argv):
        time_step = None
        number_of_replications = None
        max_simulation_time = None
        resistance_file = None
        reactance_file = None
        inter_arrival_time = None
        simulation_directory = None
        generator_directory = None
        try:
            opts, args = getopt.getopt(argv, "hs:l:m:r:x:i:d:g:",
                                       ["time_step=", "replications=", "max_time=", "resistance_file=",
                                        "reactance_file=", "inter_arrival_time=", "simulation_directory=",
                                        "generator_directory="])
        except getopt.GetoptError:
            ChargingApplication.__argument_error()
            sys.exit(2)
        if opts.__len__() == 0:
            ChargingApplication.__argument_error()
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                ChargingApplication.__print_help(sys.stdout)
                sys.exit()
            elif opt in ("-s", "--time_step"):
                time_step = float(arg)
            elif opt in ("-l", "--replications"):
                number_of_replications = int(arg)
            elif opt in ("-m", "--max_time"):
                max_simulation_time = int(arg)
            elif opt in ("-r", "--resistance_file"):
                resistance_file = arg
            elif opt in ("-x", "--reactance_file"):
                reactance_file = arg
            elif opt in ("-i", "--inter_arrival_time"):
                inter_arrival_time = float(arg)
            elif opt in ("-d", "--simulation_directory"):
                simulation_directory = arg
            elif opt in ("-g", "--generator_directory"):
                generator_directory = arg
        ChargingApplication.run_simulation(time_step, max_simulation_time, number_of_replications, resistance_file,
                                           reactance_file, inter_arrival_time, simulation_directory,
                                           generator_directory)

    @staticmethod
    def __print_help(output):
        print(
            'chargingMain.py '
            '\n-s/--time_step <simulation step>'
            '\n-l/--replications <number of replications>'
            '\n-m/--max_time <maximum simulation time>'
            '\n-r/--resistance_file <resistance file> '
            '\n-x/--reactance_file <reactance file>'
            '\n-i/--inter_arrival_time <average time between arrivals of two vehicles>'
            '\n-d/--simulation_directory <directory where simulation files are stored>'
            '\n-g/--generator_directory <path to the file with car type generator settings>', output)

    @staticmethod
    def __argument_error():
        print('Error in arguments!', file=sys.stderr)
        ChargingApplication.__print_help(sys.stderr)

    @staticmethod
    def run_simulation(time_step, max_simulation_time, number_of_replications, resistance_file,
                       reactance_file, inter_arrival_time, simulation_directory, generator_directory):
        network = Network(resistance_file, reactance_file, 1)
        network.init_electric_network()
        optimizer = NetworkOptimizer(network, 0.1)
        strategy_generator = CarTypeGenerator(None, None)
        strategy_generator.init_from_file(generator_directory, StrategiesLambdaResolver(time_step))
        if simulation_directory is not None and not os.path.exists(simulation_directory):
            os.makedirs(simulation_directory)
            os.chdir(simulation_directory)
            simulation_observer = ReplicationSimulationObserver()
            scanning_simulation_core = ChargingScanningCore(time_step, network, optimizer)
            event_simulation_core = ExperimentWithRandomArrivals(network, strategy_generator, inter_arrival_time,
                                                                 number_of_replications > 1)
            if number_of_replications == 1:
                charging_process_observer = ChargingSimulationObserver("simulationLog.txt")
                scanning_simulation_core.register_simulation_listener(charging_process_observer)
                event_simulation_core.register_simulation_listener(charging_process_observer)
            event_simulation_core.register_simulation_listener(simulation_observer)
            event_simulation_core.activity_scanning_simulation_core = scanning_simulation_core
            scanning_simulation_core.event_simulation_core = event_simulation_core
            event_simulation_core.run(number_of_replications, max_simulation_time)

        else:
            print("Directory already exists!\nSimulation will not run!")


if __name__ == '__main__':
    application = ChargingApplication()
    application.run(sys.argv[1:])
