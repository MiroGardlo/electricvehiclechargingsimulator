import abc

from model.abstractCar import Car


class RandomAbstractUser(Car, metaclass=abc.ABCMeta):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 initial_wi_generator):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time)
        self.__wi_generator = initial_wi_generator
        self._willingness_to_pay = self.generate_random_willingness_to_pay()

    def generate_random_willingness_to_pay(self):
        return self.__wi_generator.generate_sample()
