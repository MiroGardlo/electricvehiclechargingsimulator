from model.abstractCar import Car


class IncrementalTestUser(Car):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 increment_by):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time)
        self.__increment_by = increment_by

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        self._willingness_to_pay += self.__increment_by

    def _should_leave_network(self, simulation_time):
        return False
