from model.strategies.experimentalStrategies.randomAbstractUser import RandomAbstractUser
from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class RandomUserWithLocalChanges(RandomAbstractUser):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 initial_wi_generator, lower_bound_change, upper_bound_change):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         initial_wi_generator)
        self.__delta_generator = UniformRNG(lower_bound_change, upper_bound_change)

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        self._willingness_to_pay += self.__delta_generator.generate_sample()
        self._willingness_to_pay = max(self._willingness_to_pay, 0)

    def _should_leave_network(self, simulation_time):
        return False
