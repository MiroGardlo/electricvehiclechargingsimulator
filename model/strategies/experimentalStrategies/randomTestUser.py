from model.strategies.experimentalStrategies.randomAbstractUser import RandomAbstractUser


class RandomTestUser(RandomAbstractUser):

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        self._willingness_to_pay = RandomAbstractUser.generate_random_willingness_to_pay(self)

    def _should_leave_network(self, simulation_time):
        return False

