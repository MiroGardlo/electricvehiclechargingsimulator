from model.abstractCar import Car


class ResponsiveInflexibleUserCar(Car):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_network_time, max_line_following_time, kappa):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_network_time)
        self._strategy_name = "Responsive inflexible user"
        self.__current_expected_charging_time = None
        self.__max_line_following_time = max_line_following_time
        self.__start_of_charging_aim = None
        self.__kappa = kappa

    def __charging_time(self, simulation_time):
        return simulation_time - self._car_arrival_time

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        if self.__current_expected_charging_time is None:
            self.__start_of_charging_aim = simulation_time
            self.__recompute_charging_aim(simulation_time)
        if simulation_time - self.__start_of_charging_aim >= self.__max_line_following_time:
            self.__start_of_charging_aim = simulation_time
            self.__recompute_charging_aim(simulation_time)
        self._willingness_to_pay = max(self.__compute_linear_wi(simulation_time, current_time_change), 0)

    def _should_leave_network(self, simulation_time):
        return False

    def __charging_below_expectation(self, simulation_time):
        return self._current_battery_state - self._max_battery_capacity * self.__charging_time(
            simulation_time) / self._maximum_network_time < 0

    def __recompute_charging_aim(self, simulation_time):
        if not self.__charging_below_expectation(simulation_time):
            self.__current_expected_charging_time = self.__charging_time(simulation_time) \
                                                    * self._max_battery_capacity / self._current_battery_state
        else:
            self.__current_expected_charging_time = self._maximum_network_time

    def __compute_linear_wi(self, simulation_time, current_time_change):
        charging_time = self.__charging_time(simulation_time)
        return self._willingness_to_pay - self.__kappa * current_time_change * (
            self._current_battery_state - self._max_battery_capacity * charging_time / self.__current_expected_charging_time)
