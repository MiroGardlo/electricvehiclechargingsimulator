from model.abstractCar import Car


class FlexibleUserCar(Car):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_network_time, kappa):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_network_time)
        self._strategy_name = "Flexible user"
        self.__kappa = kappa

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        if not self.is_end_of_charging_time(simulation_time):
            minimal_willingness_to_pay = self.__compute_minimum_wi(simulation_time)
            max_willingness_to_pay = 1000 * minimal_willingness_to_pay
            linear_willingness_to_pay = self.__compute_linear_wi(simulation_time, current_time_change)
            self._willingness_to_pay = self.__process_parameters(minimal_willingness_to_pay, linear_willingness_to_pay,
                                                                 max_willingness_to_pay)

    def _should_leave_network(self, simulation_time):
        return False

    def __compute_minimum_wi(self, simulation_time):
        remaining_time = self._maximum_network_time - (simulation_time - self._car_arrival_time)
        denominator = 1 + ((self._max_battery_capacity - self._current_battery_state) / self._max_battery_capacity)
        return denominator / remaining_time

    def __compute_linear_wi(self, simulation_time, current_time_change):
        charging_time = simulation_time - self._car_arrival_time
        return self._willingness_to_pay - self.__kappa * current_time_change * (
            self._current_battery_state - self._max_battery_capacity * charging_time / self._maximum_network_time)

    @staticmethod
    def __process_parameters(minimal_willingness_to_pay, linear_willingness_to_pay, max_willingness_to_pay):
        if minimal_willingness_to_pay <= linear_willingness_to_pay <= max_willingness_to_pay:
            return linear_willingness_to_pay
        if linear_willingness_to_pay < minimal_willingness_to_pay:
            return minimal_willingness_to_pay
        return max_willingness_to_pay
