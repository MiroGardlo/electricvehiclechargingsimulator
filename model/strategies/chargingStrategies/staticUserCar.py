from model.abstractCar import Car


class StaticUserCar(Car):
    """
    Class represents a static user whe never changes his/her
    willingness to pay parameter over time
    """

    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_network_time=None):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_network_time)
        self._strategy_name = "Static user"

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        # static user doesn't change his/her willingness to pay
        pass

    def _should_leave_network(self, simulation_time):
        return False
