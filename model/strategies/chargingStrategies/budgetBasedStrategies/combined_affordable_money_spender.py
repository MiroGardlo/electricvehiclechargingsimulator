from model.strategies.chargingStrategies.budgetBasedStrategies.budget_based_user_car import BudgetBasedUserCar


class CombinedAffordableMoneySpender(BudgetBasedUserCar):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_budget, maximum_charging_time, decision_interval, kappa,
                 minimum_willingness_to_pay, spending_time_fraction):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_budget, maximum_charging_time, decision_interval)
        self._strategy_name = "Combined affordable price spender"
        self.__kappa = kappa
        if not 0 <= spending_time_fraction <= 1:
            raise ValueError("Spending time fraction can only be in interval <0, 1>")
        self.__spending_time_fraction = spending_time_fraction
        self.__minimum_willingness_to_pay = minimum_willingness_to_pay
        self.__compute_trajectory(maximum_charging_time, spending_time_fraction)

    def set_maximum_network_time(self, maximum_network_time):
        super().set_maximum_network_time(maximum_network_time)
        self.__compute_trajectory(maximum_network_time, self.__spending_time_fraction)

    def __compute_trajectory(self, maximum_charging_time, spending_time_fraction):
        trajectory = self.__compute_fraction_trajectory(maximum_charging_time, spending_time_fraction)
        self.__slope = trajectory[0]
        self.__offset = trajectory[1]

    def compute_willingness_to_pay(self, simulation_time, current_time_change):
        if not self.is_end_of_charging_time(simulation_time):
            charging_time = simulation_time - self.car_arrival_time
            remaining_capacity = self._max_battery_capacity - self._current_battery_state
            computed_wi_budget = self.__compute_affordable_wi(remaining_capacity, current_time_change)
            computed_wi_energy = self.__compute_energy_based_willingness_to_pay(charging_time)
            self._willingness_to_pay = max(computed_wi_budget, computed_wi_energy, self.__minimum_willingness_to_pay)

    def __compute_affordable_wi(self, remaining_capacity, current_time_change):
        affordable_price = self._current_budget / remaining_capacity
        paid_price = self._willingness_to_pay * current_time_change / self._currently_assigned_power
        return self._willingness_to_pay + self.__kappa * current_time_change * (affordable_price - paid_price)

    def __compute_energy_based_willingness_to_pay(self, charging_time):
        current_fraction = self.__compute_current_fraction(charging_time)
        remaining_time = self._maximum_network_time - charging_time
        return current_fraction * self._current_budget / remaining_time

    def __compute_current_fraction(self, charging_time):
        return self.__slope * charging_time + self.__offset

    @staticmethod
    def __compute_fraction_trajectory(maximum_charging_time, spending_time_fraction):
        k = 1 / (maximum_charging_time * (1 - spending_time_fraction))
        q = -spending_time_fraction / (1 - spending_time_fraction)
        return k, q
