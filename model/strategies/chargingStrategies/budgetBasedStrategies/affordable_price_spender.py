from model.strategies.chargingStrategies.budgetBasedStrategies.budget_based_user_car import BudgetBasedUserCar


class AffordablePriceSpender(BudgetBasedUserCar):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_budget, maximum_charging_time, decision_interval, kappa, minimum_willingness_to_pay):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_budget, maximum_charging_time, decision_interval)
        self._strategy_name = "Affordable price spender"
        self.__kappa = kappa
        self.__minimum_willingness_to_pay = minimum_willingness_to_pay

    def compute_willingness_to_pay(self, simulation_time, current_time_change):
        remaining_capacity = self._max_battery_capacity - self._current_battery_state
        computed_wi = self.__compute_next_willingness_to_pay(remaining_capacity, current_time_change)
        self._willingness_to_pay = max(self.__minimum_willingness_to_pay, computed_wi)

    def __compute_next_willingness_to_pay(self, remaining_capacity, current_time_change):
        affordable_price = self._current_budget / remaining_capacity
        paid_price = self._willingness_to_pay * current_time_change / self._currently_assigned_power
        return self._willingness_to_pay + self.__kappa * current_time_change * (affordable_price - paid_price)
