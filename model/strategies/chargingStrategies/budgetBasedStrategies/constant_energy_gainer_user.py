from model.strategies.chargingStrategies.budgetBasedStrategies.budget_based_user_car import BudgetBasedUserCar


class ConstantEnergyGainerUser(BudgetBasedUserCar):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_budget, maximum_charging_time, decision_interval, kappa):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_budget, maximum_charging_time, decision_interval)
        self._strategy_name = "Uniform charging in time"
        self.__kappa = kappa

    def compute_willingness_to_pay(self, simulation_time, current_time_change):
        linear_wi = self.__compute_linear_wi(simulation_time, current_time_change)
        self._willingness_to_pay = max(linear_wi, 0)

    def __compute_linear_wi(self, simulation_time, current_time_change):
        charging_time = simulation_time - self._car_arrival_time
        return self._willingness_to_pay - self.__kappa * current_time_change * (
            self._current_battery_state - self._max_battery_capacity * charging_time / self._maximum_network_time)
