import abc

from model.abstractCar import Car
from model.elementHistoryInfo import ElementHistoryInfo
from model.time_info import TimeInfo


class BudgetBasedUserCar(Car, metaclass=abc.ABCMeta):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_budget, maximum_charging_time, decision_interval):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_charging_time, decision_interval)
        self.__maximum_budget = maximum_budget
        self._current_budget = maximum_budget
        self.__budget_history = None
        self._time_of_last_battery_update = None
        self._currently_assigned_power = None

    @property
    def current_budget(self):
        return self._current_budget

    @property
    def maximum_budget(self):
        return self.__maximum_budget

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        self.compute_willingness_to_pay(simulation_time, current_time_change)
        if self._decision_interval is not None:
            maximum_price_to_pay = self._current_budget / self._decision_interval
            if self._willingness_to_pay > maximum_price_to_pay:
                self._willingness_to_pay = maximum_price_to_pay

    @abc.abstractclassmethod
    def compute_willingness_to_pay(self, simulation_time, current_time_change):
        pass

    def set_maximum_budget(self, maximum_budget):
        if self.__budget_history is not None:
            raise ValueError("Budget cannot be altered during simulation")
        else:
            self._current_budget = maximum_budget
            self.__maximum_budget = maximum_budget

    def set_car_arrival_time(self, arrival_time):
        super().set_car_arrival_time(arrival_time)
        self.__budget_history = ElementHistoryInfo()
        self.__budget_history.add_element_time_info(TimeInfo(arrival_time, self._current_budget))

    def update_battery_capacity(self, simulation_time, assigned_power):
        super().update_battery_capacity(simulation_time, assigned_power)
        self.__update_budget(simulation_time)
        self._currently_assigned_power = assigned_power

    def write_car_to_file(self):
        super().write_car_to_file()
        state_info = "charged" if self.is_fully_charged() else "not_charged"
        self._print_evolution_information(state_info, "_budget_evolution", self.__budget_history)

    def _should_leave_network(self, simulation_time):
        return self._current_budget <= 0

    def __update_budget(self, simulation_time):
        time_of_last_change = self._time_of_last_battery_update \
            if self._time_of_last_battery_update is not None \
            else self._car_arrival_time
        current_charging_period = simulation_time - time_of_last_change
        self._current_budget -= self._willingness_to_pay * current_charging_period
        self._time_of_last_battery_update = simulation_time
        self.__budget_history.add_element_time_info(TimeInfo(simulation_time, self._current_budget))
