from model.strategies.chargingStrategies.budgetBasedStrategies.budget_based_user_car import BudgetBasedUserCar


class ConstantPerTimeSpender(BudgetBasedUserCar):
    def __init__(self, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_budget, maximum_charging_time):
        initial_willingness_to_pay = maximum_budget / maximum_charging_time
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                         maximum_budget, maximum_charging_time, None)
        self._strategy_name = "Uniform in time spender"

    def set_maximum_budget(self, maximum_budget):
        super().set_maximum_budget(maximum_budget)
        self._willingness_to_pay = maximum_budget / self.maximum_network_time

    def set_maximum_network_time(self, maximum_network_time):
        super().set_maximum_network_time(maximum_network_time)
        self._willingness_to_pay = self._current_budget / self.maximum_network_time

    def compute_willingness_to_pay(self, simulation_time, current_time_change):
        # this user keeps initial willingness to pay in all situations
        pass
