from model.abstractCar import Car


class StaticInflexibleUserCar(Car):
    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_network_time, kappa):
        super().__init__(initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time, maximum_network_time)
        self._strategy_name = "Static inflexible user"
        self.__kappa = kappa

    def __charging_time(self, simulation_time):
        return simulation_time - self._car_arrival_time

    def update_willingness_to_pay(self, simulation_time, current_time_change):
        if self.__charging_below_expectation(simulation_time):
            linear_wi = self.__compute_linear_wi(simulation_time, current_time_change)
            self._willingness_to_pay = max(linear_wi, 0)

    def _should_leave_network(self, simulation_time):
        return False

    def __compute_linear_wi(self, simulation_time, current_time_change):
        return self._willingness_to_pay - self.__kappa * current_time_change * (
            self._current_battery_state - self._max_battery_capacity * self.__charging_time(
                simulation_time) / self._maximum_network_time)

    def __charging_below_expectation(self, simulation_time):
        return self._current_battery_state - self._max_battery_capacity * self.__charging_time(
            simulation_time) / self._maximum_network_time < 0
