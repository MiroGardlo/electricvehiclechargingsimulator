class ElementHistoryInfo:
    """
        Class represents a history of battery state evolution over time
    """

    def __init__(self):
        self.element_history = []

    def add_element_time_info(self, element_state_info):
        """
        Adds new battery info to the history
        :param element_state_info:
        :return: None
        """
        self.element_history.append(element_state_info)

    def __str__(self):
        return "\n".join(str(x) for x in self.element_history)

    def reset_history(self):
        self.element_history = []
