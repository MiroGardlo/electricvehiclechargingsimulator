import abc

from model.elementHistoryInfo import ElementHistoryInfo
from model.time_info import TimeInfo
from simulation.simulationUtils.weighted_statistics import WeightedStatistics


class Car(metaclass=abc.ABCMeta):
    __current_car_id = 0

    def __init__(self, initial_willingness_to_pay, current_battery_state, max_battery_capacity, arrival_time,
                 maximum_network_time=None, decision_interval=None):
        self.__car_id = self.__assign_car_id()
        self.__willingness_to_pay_statistics = WeightedStatistics()
        self._car_arrival_time = arrival_time
        self._willingness_to_pay = initial_willingness_to_pay
        self._current_battery_state = current_battery_state
        self._battery_state_history = ElementHistoryInfo()
        self._willingness_to_pay_history = ElementHistoryInfo()
        self._max_battery_capacity = max_battery_capacity
        self._battery_state_history.add_element_time_info(TimeInfo(arrival_time, current_battery_state))
        self._willingness_to_pay_history.add_element_time_info(TimeInfo(arrival_time, self._willingness_to_pay))
        self.__connected_node = None
        self._strategy_name = None
        self._maximum_network_time = maximum_network_time
        self._decision_interval = decision_interval

    @property
    def maximum_network_time(self):
        return self._maximum_network_time

    @property
    def car_id(self):
        return self.__car_id

    @property
    def is_in_network(self):
        return self.__connected_node is not None

    @property
    def charger_node(self):
        return self.__connected_node

    @property
    def max_battery_capacity(self):
        return self._max_battery_capacity

    @property
    def willingness_to_pay(self):
        return self._willingness_to_pay

    @property
    def car_arrival_time(self):
        return self._car_arrival_time

    @property
    def decision_interval(self):
        return self._decision_interval

    @decision_interval.setter
    def decision_interval(self, value):
        self._decision_interval = value

    @property
    def current_battery_state(self):
        return self._current_battery_state

    @property
    def willingness_to_pay_statistics(self):
        return self.__willingness_to_pay_statistics

    @property
    def battery_state_history(self):
        return self._battery_state_history

    @abc.abstractclassmethod
    def update_willingness_to_pay(self, simulation_time, current_time_change):
        # Update willingness to pay according to a specific strategy
        pass

    @abc.abstractclassmethod
    def _should_leave_network(self, simulation_time):
        pass

    def set_maximum_network_time(self, maximum_network_time):
        if self.__connected_node is not None:
            raise ValueError("Maximum network time cannot be altered after connection to node")
        else:
            self._maximum_network_time = maximum_network_time

    def set_car_arrival_time(self, arrival_time):
        self._battery_state_history = ElementHistoryInfo()
        self._battery_state_history.add_element_time_info(TimeInfo(arrival_time, self._current_battery_state))
        self._willingness_to_pay_history = ElementHistoryInfo()
        self._willingness_to_pay_history.add_element_time_info(TimeInfo(arrival_time, self._willingness_to_pay))
        self.__willingness_to_pay_statistics = WeightedStatistics(arrival_time)
        self._car_arrival_time = arrival_time

    def write_car_to_file(self):
        state_info = "charged" if self.is_fully_charged() else "not_charged"
        self.__print_basic_car_information(state_info)
        self._print_evolution_information("_wi_evolution", state_info, self._willingness_to_pay_history)
        self._print_evolution_information("_battery_evolution", state_info, self._battery_state_history)

    def connect_to_node(self, node):
        self.__connected_node = node

    def disconnect_from_node(self):
        self.__connected_node.disconnect_car(self.__car_id)
        self.__connected_node = None

    def assigned_power_exceeds_max_capacity(self, assigned_power):
        """
        Check whether the car is supposed to leave the network after assigning
        specified power
        :param assigned_power: amount of power assigned to electric car
        :return: true if car will be charged fully after updating battery capacity, false otherwise
        """
        if self._current_battery_state + assigned_power >= self._max_battery_capacity:
            return True
        return False

    def update_battery_capacity(self, simulation_time, assigned_power):
        """
            Update battery capacity if assigned power is over the limit assign maximal
        """
        self.__update_average_willingness_to_pay(simulation_time)
        if not self.assigned_power_exceeds_max_capacity(assigned_power):
            self._current_battery_state += assigned_power
        else:
            self._current_battery_state = self._max_battery_capacity
        self._battery_state_history.add_element_time_info(TimeInfo(simulation_time, self.current_battery_state))

    def trigger_willingness_to_pay_update(self, simulation_time, current_time_change):
        if not self.is_ready_to_leave_network(simulation_time) and not self.__is_t_max_elapsed(simulation_time):
            wi_before_update = self._willingness_to_pay
            self.update_willingness_to_pay(simulation_time, current_time_change)
            self.__connected_node.update_nodal_wi(wi_before_update, self._willingness_to_pay)
        self._willingness_to_pay_history.add_element_time_info(TimeInfo(simulation_time, self._willingness_to_pay))

    def is_ready_to_leave_network(self, simulation_time):
        return self.is_fully_charged() or self._should_leave_network(simulation_time)

    def __is_t_max_elapsed(self, simulation_time):
        if self.maximum_network_time is not None:
            return self.is_end_of_charging_time(simulation_time)
        return False

    def is_end_of_charging_time(self, simulation_time):
        charging_time = simulation_time - self.car_arrival_time
        return abs(charging_time - self._maximum_network_time) <= 10e-8

    def is_fully_charged(self):
        return self._current_battery_state == self._max_battery_capacity

    def __update_average_willingness_to_pay(self, simulation_time):
        self.__willingness_to_pay_statistics.update_observation(self._willingness_to_pay, simulation_time)

    @staticmethod
    def __assign_car_id():
        Car.__current_car_id += 1
        return Car.__current_car_id

    def __print_basic_car_information(self, file_info):
        car_file = open("car%d_%s" % (self.__car_id, file_info), 'w')
        car_file.write("Car strategy: %s \n" % self._strategy_name)
        car_file.write("Arrival time: %f \n" % self._car_arrival_time)
        car_file.write("Average willingness to pay: %f\n" % self.__willingness_to_pay_statistics.get_average())
        car_file.write("Charger node number: %d\n" % self.__connected_node.node_number)
        car_file.write("Charger node level: %d\n" % self.__connected_node.node_level)
        car_file.write("Maximum possible battery state: %.8f\n" % self.max_battery_capacity)
        if self.maximum_network_time is not None:
            car_file.write("Maximum network time: %.8f\n" % self.maximum_network_time)
        car_file.close()

    def _print_evolution_information(self, state_info, filename_suffix, evolution_info):
        car_file = open("car%d_%s_%s" % (self.__car_id, state_info, filename_suffix), 'w')
        car_file.write(str(evolution_info))
        car_file.close()
