class TimeInfo:
    """
        Class represents information about state of a specific element in time. It is pair
        consisting from simulation time and the actual state of specific element
    """

    def __init__(self, simulation_time, element_state):
        self.__simulation_time = simulation_time
        self.__element_state = element_state

    @property
    def simulation_time(self):
        return self.__simulation_time

    @property
    def element_state(self):
        return self.__element_state

    def __str__(self):
        return "%f,%f" % (self.__simulation_time, self.__element_state)
