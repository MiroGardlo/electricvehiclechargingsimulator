number_of_nodes = 4;
cvx_begin
    variable x(number_of_nodes);
    maximize log(x(1)) + log(x(2)) + log(x(3)) + log(x(4));
    x(4) <= 1;
    x(3) + x(4) <= 1;
    x(2) + x(3) + x(4) <= 1;
    x(1) + x(2) + x(3) + x(4) <= 1;
cvx_end
disp(x);