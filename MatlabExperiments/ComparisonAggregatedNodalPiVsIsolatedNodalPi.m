
clear all

% parameters
alpha = 0.1;                    % allowable voltage deviation
Vnom  = 1;                      % Nominal Voltage

% imput
R = dlmread('R.txt');
X = dlmread('X.txt');
Cars = dlmread('C2.txt');
% output
optimization_output = fopen('C:\Users\Miroslav\Desktop\Univerzita\4.rocnik\1.semester\Optimalizacia el. siete\source_code\source_code\Comparison Aggregated vs specific\result.txt','w');


[r1, r2] = size(R);
[x1, x2] = size(X);
[c1, c2] = size(Cars);

if (r1 == r2) &&  (x1 == x2) && (r1 == x1) && (c1 == r1 - 1)
    n             = r1;                       % number of nodes


    % auxiliary data structures
    adj_nodes     = {};                       % list of list of neighbouting nodes

    edges_indexes = zeros(n,n);


    % build list of adjacent nodes for each node
    index = 1;
    for i = 1:n
        L = [];
        for j = 1:n
            if ((R(i,j) > 0) || (X(i,j) > 0))
               L = [L j];
            end
            if ((R(i,j) > 0)||X(i,j) > 0) && (i > j)
               edges_indexes(i,j) = index;
               edges_indexes(j,i) = index;
               index = index + 1;
            end
        end
        adj_nodes{i} = L;
    end

    % information about cars
    number_of_cars          = c2;

    %impedancia
    Z  = complex(R,X);
    wi = ones(1,number_of_cars);

    Wii_RE_factors_pij = zeros(n-1,n);
    Wii_RE_factors_qij = zeros(n-1,n);
    Wij_RE_factors_pij = zeros(n-1,n-1);
    Wij_IM_factors_pij = zeros(n-1,n-1);
    Wij_RE_factors_qij = zeros(n-1,n-1);
    Wij_IM_factors_qij = zeros(n-1,n-1);

    for i = 2:n
        for j = adj_nodes{i}
            edge = edges_indexes(i,j);
            temp = R(i,j)^2 + X(i,j)^2;
            gij  = R(i,j)/temp;
            bij  = X(i,j)/temp;
            % matica - pre kazdy vrchol i vyrobim gij na spravne pozicie
            Wii_RE_factors_pij(i-1,i)      =  Wii_RE_factors_pij(i-1,i)    + gij;
            Wii_RE_factors_qij(i-1,i)      =  Wii_RE_factors_qij(i-1,i)    + bij;
            Wij_RE_factors_pij(i-1,edge)   =  Wij_RE_factors_pij(i-1,edge) - gij;
            Wij_RE_factors_qij(i-1,edge)   =  Wij_RE_factors_qij(i-1,edge) - bij;
            if (i < j)
                Wij_IM_factors_pij(i-1,edge)   =  Wij_IM_factors_pij(i-1,edge) + bij;
                Wij_IM_factors_qij(i-1,edge)   =  Wij_IM_factors_qij(i-1,edge) - gij;
             else
                Wij_IM_factors_pij(i-1,edge)   =  Wij_IM_factors_pij(i-1,edge) - bij;
                Wij_IM_factors_qij(i-1,edge)   =  Wij_IM_factors_qij(i-1,edge) + gij;
             end
        end
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                      SIMULATION LOOP                               %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % select solver
    %cvx_solver sedumi     % version 1.34
    % cvx_solver gurobi
    % cvx_solver sdpt3
    % cvx_solver mosek

    % Enter CVX environment
      %Enter CVX environment
   for car=1:number_of_cars
      for loop=1:1
        disp(sprintf('Current car: %d, current wi: %d\n', car, loop));

        wi(car) = 100;
        aggregated_nodal_wi = [];
        for i=1:n-1
            aggregated_nodal_wi(i) = dot(wi, Cars(i,:));
        end
            cvx_begin quiet
            cvx_precision best

            variable nodal_pi(number_of_cars)                  % real power charging vehicle
            variable Wij_RE(n-1)                    % real part of W_ij for all edges
            variable Wij_IM(n-1)                    % imaginary part of W_ij for all edges
            variable Wii_RE(n)                      % real part of W_ii for all nodes

            dual     variable lambda_1{n-1}
            dual     variable lambda_2{n-1}

            maximise wi*log(-nodal_pi)

            subject to
            % nodal power balance ku kazdej dve lambdy
            % dualna premenna - do akej miery je ta podmienka zavazujuca -
            % citlivost
            %lambda dualna premenna
            for i = 2:n
                %fix this
                lambda_1{i-1}:  dot(Wii_RE_factors_pij(i-1,:),Wii_RE)  + dot(Wij_RE_factors_pij(i-1,:), Wij_RE)   + dot(Wij_IM_factors_pij(i-1,:), Wij_IM) == dot(nodal_pi, Cars(i-1,:));
                lambda_2{i-1}:  dot(Wii_RE_factors_qij(i-1,:),Wii_RE)  + dot(Wij_RE_factors_qij(i-1,:), Wij_RE)   + dot(Wij_IM_factors_qij(i-1,:), Wij_IM) == 0;
            end

            % voltage limits
            for i = 1:n
                ((1-alpha)*Vnom)^2 <= Wii_RE(i) <= ((1+alpha)*Vnom)^2;
            end

            % bounds
            for i = 1:n-1
                nodal_pi(i) <= 0;
            end

            for i = 1:n
                for j = adj_nodes{i}
                    if (i < j)
                        edge = edges_indexes(i,j);
                        norm([2*Wij_RE(edge), 2*Wij_IM(edge), (Wii_RE(i) - Wii_RE(j)) ])  <= Wii_RE(i) + Wii_RE(j);
                    end
                end
            end
            cvx_end


            %aggregated implementation
            cvx_begin quiet
            cvx_precision best

            variable nodal_pi_aggregated(n-1)                  % real power charging vehicle
            variable Wij_RE_agg(n-1)                    % real part of W_ij for all edges
            variable Wij_IM_agg(n-1)                    % imaginary part of W_ij for all edges
            variable Wii_RE_agg(n)                      % real part of W_ii for all nodes

            dual     variable lambda_1_agg{n-1}
            dual     variable lambda_2_agg{n-1}

            maximise aggregated_nodal_wi*log(-nodal_pi_aggregated)

            subject to
            %lambda dualna premenna
            for i = 2:n
                lambda_1_agg{i-1}:  dot(Wii_RE_factors_pij(i-1,:),Wii_RE_agg)  + dot(Wij_RE_factors_pij(i-1,:), Wij_RE_agg)   + dot(Wij_IM_factors_pij(i-1,:), Wij_IM_agg) == nodal_pi_aggregated(i-1);
                lambda_2_agg{i-1}:  dot(Wii_RE_factors_qij(i-1,:),Wii_RE_agg)  + dot(Wij_RE_factors_qij(i-1,:), Wij_RE_agg)   + dot(Wij_IM_factors_qij(i-1,:), Wij_IM_agg) == 0;
            end

            % voltage limits
            for i = 1:n
                ((1-alpha)*Vnom)^2 <= Wii_RE_agg(i) <= ((1+alpha)*Vnom)^2;
            end

            % bounds
            for i = 1:n-1
                nodal_pi_aggregated(i) <= 0;
            end

            for i = 1:n
                for j = adj_nodes{i}
                    if (i < j)
                        edge = edges_indexes(i,j);
                        norm([2*Wij_RE_agg(edge), 2*Wij_IM_agg(edge), (Wii_RE_agg(i) - Wii_RE_agg(j)) ])  <= Wii_RE_agg(i) + Wii_RE_agg(j);
                    end
                end
            end
            cvx_end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % validate rank-1 constraint
            visited         = zeros(1, n);
            stack           =  [1];
            stack_size      = size(stack);
            visited(1)      = 1;
            while stack_size(2) > 0
                i           = stack(1);
                stack(1)    = [];
                for j = adj_nodes{i}
                    if (visited(j) == 0)
                        stack = [stack j];
                        visited(j)  = 1;

                        if (i < j)
                            if abs( complex(Wij_RE_agg(edges_indexes(i,j)),Wij_IM_agg(edges_indexes(i,j)))*conj(complex(Wij_RE_agg(edges_indexes(i,j)),Wij_IM_agg(edges_indexes(i,j)))) - Wii_RE_agg(i)*Wii_RE_agg(j)) > 1.e-8
                                disp('Rank different from 1 has been detected');
                            end
                        else
                            if abs( complex(Wij_RE_agg(edges_indexes(i,j)),-Wij_IM_agg(edges_indexes(i,j)))*conj(complex(Wij_RE_agg(edges_indexes(i,j)),-Wij_IM_agg(edges_indexes(i,j)))) - Wii_RE_agg(i)*Wii_RE_agg(j)) > 1.e-8
                                disp('Rank different from 1 has been detected');
                            end
                        end
                    end
                end
                stack_size = size(stack);
            end

        if ((exist('lambda_1'))&&(exist('lambda_2')))
            fprintf(optimization_output,'%6.10f ', wi);
            fprintf(optimization_output,'\n');
            fprintf(optimization_output,'%6.10f ', -nodal_pi);
            fprintf(optimization_output,'\n');
            aggregated_power = [];
            for i=1:n-1
                current_node = Cars(i,:);
                if norm(current_node) ~= 0
                    car_wi_for_node = times(current_node,wi);
                    wi_fraction = car_wi_for_node/aggregated_nodal_wi(i);
                    %delete zero elements
                    wi_fraction(wi_fraction==0) = [];
                    assigned_power_aggregated = -nodal_pi_aggregated(i)*wi_fraction;
                    aggregated_power = [aggregated_power assigned_power_aggregated];
                end
            end
            fprintf(optimization_output,'%6.10f ', aggregated_power);
            fprintf(optimization_output,'\n');
            fprintf(optimization_output,'%6.10f ', abs(transpose(aggregated_power) + nodal_pi));
        end
        fprintf(optimization_output,'\n');
        fprintf(optimization_output,'\n');
      end
      wi(car) = 1;
   end
else
    disp('Inconsistent size of input matrices');
end
format long

fclose(optimization_output);
disp('Simulation completed...')
