import getopt
import os
import sys

from ExperimentResolver import ExperimentResolver
from electricNetwork.network import Network
from simulation.experiments.simulationExperiments.realChargingScanning import ChargingScanningCore
from simulation.optimizer import NetworkOptimizer
from simulation.simulationCore.charging_simulation_logger import ChargingSimulationObserver
from simulation.simulationCore.replication_simulation_logger import ReplicationSimulationObserver
from simulation.simulationUtils.generators.carTypeGenerator.car_type_generator import CarTypeGenerator
from simulation.simulationUtils.generators.carTypeGenerator.strategies_lambda_resolver import StrategiesLambdaResolver


class ChargingApplication:
    @staticmethod
    def run(argv):
        time_step = None
        max_simulation_time = None
        resistance_file = None
        reactance_file = None
        simulation_directory = None
        generator_directory = None
        scenario_number = None
        try:
            opts, args = getopt.getopt(argv, "hs:m:r:x:d:g:n:",
                                       ["time_step=", "max_time=", "resistance_file=",
                                        "reactance_file=", "simulation_directory=",
                                        "generator_directory=", "scenario_number="])
        except getopt.GetoptError:
            ChargingApplication.__argument_error()
            sys.exit(2)
        if opts.__len__() == 0:
            ChargingApplication.__argument_error()
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                ChargingApplication.__print_help(sys.stdout)
                sys.exit()
            elif opt in ("-s", "--time_step"):
                time_step = float(arg)
            elif opt in ("-m", "--max_time"):
                max_simulation_time = int(arg)
            elif opt in ("-r", "--resistance_file"):
                resistance_file = arg
            elif opt in ("-x", "--reactance_file"):
                reactance_file = arg
            elif opt in ("-d", "--simulation_directory"):
                simulation_directory = arg
            elif opt in ("-g", "--generator_directory"):
                generator_directory = arg
            elif opt in ("-n", "--scenario_number"):
                scenario_number = int(arg)
        ChargingApplication.run_simulation(time_step, max_simulation_time, resistance_file,
                                           reactance_file, simulation_directory,
                                           generator_directory, scenario_number)

    @staticmethod
    def __print_help(output):
        print(
            'nonRandomChargingMain.py\n'
            '-s/--time_step <simulation step>\n'
            '-m/--max_time <maximum simulation time>\n'
            '-r/--resistance_file <resistance_file>\n'
            '-x/--reactance_file <reactance_file>\n'
            '-d/--simulation_directory <directory where simulation files are stored>\n'
            '-g/--generator_directory <path to the file with car type generator settings>\n'
            '-n/--scenario_number <simulation scenario number>\n', file=output)

    @staticmethod
    def __argument_error():
        print('Error in arguments!', file=sys.stderr)
        ChargingApplication.__print_help(sys.stderr)

    @staticmethod
    def run_simulation(time_step, max_simulation_time, resistance_file,
                       reactance_file, simulation_directory, generator_directory, scenario_number):
        network = Network(resistance_file, reactance_file, 1)
        network.init_electric_network()
        optimizer = NetworkOptimizer(network, 0.1)
        strategy_generator = CarTypeGenerator(None, None)
        strategy_generator.init_from_file(generator_directory, StrategiesLambdaResolver(time_step))
        if simulation_directory is not None and not os.path.exists(simulation_directory):
            os.makedirs(simulation_directory)
            os.chdir(simulation_directory)
            ChargingApplication.prepare_simulation(max_simulation_time, network, optimizer, strategy_generator,
                                                   time_step, scenario_number)
        else:
            print("Directory already exists!\nSimulation will not run!")

    @staticmethod
    def prepare_simulation(max_simulation_time, network, optimizer, strategy_generator, time_step, scenario_number):
        simulation_observer = ReplicationSimulationObserver()
        scanning_simulation_core = ChargingScanningCore(time_step, network, optimizer)
        experiment_resolver = ExperimentResolver()
        experiment_resolver.prepare_simulation_scenarios(network, strategy_generator)
        event_simulation_core = experiment_resolver.resolve_simulation_scenario(scenario_number)
        charging_process_observer = ChargingSimulationObserver("simulationLog.txt")
        scanning_simulation_core.register_simulation_listener(charging_process_observer)
        event_simulation_core.register_simulation_listener(charging_process_observer)
        event_simulation_core.register_simulation_listener(simulation_observer)
        event_simulation_core.activity_scanning_simulation_core = scanning_simulation_core
        scanning_simulation_core.event_simulation_core = event_simulation_core
        event_simulation_core.run(1, max_simulation_time)


if __name__ == '__main__':
    application = ChargingApplication()
    application.run(sys.argv[1:])
