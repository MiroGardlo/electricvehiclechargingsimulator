from simulation.experiments.scalingExperiments.parametersTest import ParametersTest
from simulation.experiments.scalingExperiments.scalingExperimentWithEmpiricalWi import ScalingExperimentWithEmpiricalWi
from simulation.experiments.scalingExperiments.scaling_experiment_with_side_by_side_comparison import \
    ScalingExperimentWithSideBySideComparison
from simulation.experiments.simulationExperiments.experiment_with_arrival_of_aggresive_vehicle import \
    ExperimentWithLateArrivalOfAggressiveSpender
from simulation.experiments.simulationExperiments.experiment_with_varying_budgets import ExperimentWithVariousBudgets
from simulation.experiments.simulationExperiments.experiment_with_varying_t_max import ExperimentWithVaryingTMax


class ExperimentResolver:
    def __init__(self):
        self.__scaling_experiments = None
        self.__simulation_scenarios = None

    def prepare_scaling_experiments(self, electric_network, optimizer, time_step):
        self.__scaling_experiments = [
            ScalingExperimentWithEmpiricalWi(optimizer, time_step, electric_network, -1, 1),
            ScalingExperimentWithEmpiricalWi(optimizer, time_step, electric_network, -10, 10),
            ScalingExperimentWithEmpiricalWi(optimizer, time_step, electric_network, -100, 100),
            ScalingExperimentWithSideBySideComparison(optimizer, time_step, electric_network, -1, 1),
            ScalingExperimentWithSideBySideComparison(optimizer, time_step, electric_network, -10, 10),
            ScalingExperimentWithSideBySideComparison(optimizer, time_step, electric_network, -100, 100)]

    def prepare_simulation_scenarios(self, network, strategy_generator):
        self.__simulation_scenarios = [
            ExperimentWithVariousBudgets(network, strategy_generator),
            ExperimentWithVaryingTMax(network, strategy_generator),
            ExperimentWithLateArrivalOfAggressiveSpender(network, strategy_generator)
        ]

    def resolve_scaling_experiment(self, experiment_number):
        return self.__scaling_experiments[experiment_number]

    def resolve_simulation_scenario(self, scenario_number):
        return self.__simulation_scenarios[scenario_number]
