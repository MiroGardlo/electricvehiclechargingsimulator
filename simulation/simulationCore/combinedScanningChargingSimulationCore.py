import abc

from simulation.simulationCore.combinedScanningSimulationCore import CombinedScanningSimulationCore


class CombinedScanningChargingSimulationCore(CombinedScanningSimulationCore, metaclass=abc.ABCMeta):
    def __init__(self, time_step, electric_network, optimizer):
        super().__init__(time_step)
        self._electric_network = electric_network
        self._optimizer = optimizer
