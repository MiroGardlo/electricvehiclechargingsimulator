import abc
import itertools
from queue import PriorityQueue


class EventSimulationCore(metaclass=abc.ABCMeta):
    def __init__(self):
        self.__simulation_observers = []
        self._activity_scanning_simulation_core = None
        self._simulation_time = 0
        self._event_queue = PriorityQueue()
        self.__event_counter_order = itertools.count()

    @property
    def activity_scanning_simulation_core(self):
        return self._activity_scanning_simulation_core

    @activity_scanning_simulation_core.setter
    def activity_scanning_simulation_core(self, value):
        self._activity_scanning_simulation_core = value

    def register_simulation_listener(self, simulation_observer):
        self.__simulation_observers.append(simulation_observer)

    def remove_simulation_listener(self, simulation_observer):
        self.__simulation_observers.remove(simulation_observer)

    def plan_event(self, event):
        """
        Plans event on time line of event simulation core
        :param event: event to be planned
        :return: None
        """
        if event.event_time < self._simulation_time:
            raise ValueError('Cannot plan event before current simulation time')
        else:
            order = self.__event_counter_order.__next__()
            self._event_queue.put([event.event_time, order, event])

    def run(self, number_of_replications, max_time):
        """
        Runs simulation core with specified number of replications and maximum time
        :param number_of_replications: number of replications to run
        :param max_time: maximum simulation time
        :return: None
        """
        self.before_simulation_start()
        self.__notify_observers_on_simulation_start()
        for i in range(number_of_replications):
            self._simulation_time = 0
            self.before_replication_start()
            self.__run_replication(max_time)
            self.__replication_ended(i + 1)
            self._activity_scanning_simulation_core.on_replication_end()
        self.__simulation_ended()

    def __run_replication(self, max_time):
        while self._simulation_time <= max_time and not self.__calendar_empty():
            current_event = self._event_queue.get()[2]
            self._simulation_time = current_event.event_time
            current_event.execute_action()
            self._event_queue.task_done()
            self.__notify_observers_on_event(self._simulation_time, current_event.event_detail)
            if not self.__calendar_empty():
                time_for_activities = self._event_queue.queue[0][0] - self._simulation_time
                if self._simulation_time <= max_time and self._activity_scanning_simulation_core is not None:
                    self._activity_scanning_simulation_core.run_for_time(self._simulation_time, time_for_activities)

    def __calendar_empty(self):
        return self._event_queue.qsize() == 0

    @abc.abstractclassmethod
    def before_simulation_start(self):
        """
        Defines action to be performed before starting simulation
        :return:
        """
        pass

    @abc.abstractclassmethod
    def before_replication_start(self):
        """
        Defines action to be performed before start of each replication
        :return:
        """
        pass

    @abc.abstractclassmethod
    def on_replication_end(self):
        """
        Defines action to be performed after replication finished
        :return:
        """
        pass

    @abc.abstractclassmethod
    def on_simulation_end(self):
        """
        Defines action to be performed after simulation finished
        :return:
        """
        pass

    def __notify_observers_on_event(self, simulation_time, event_detail):
        for observer in self.__simulation_observers:
            observer.on_event_occurrence(simulation_time, event_detail, self)

    def __replication_ended(self, replication_number):
        self.on_replication_end()
        self.__clear_events()
        for observer in self.__simulation_observers:
            observer.on_replication_end(replication_number, self)

    def __simulation_ended(self):
        self.on_simulation_end()
        for observer in self.__simulation_observers:
            observer.on_simulation_end(self)

    def __notify_observers_on_simulation_start(self):
        for observer in self.__simulation_observers:
            observer.on_simulation_start(self)

    def __clear_events(self):
        self._event_queue = PriorityQueue()
