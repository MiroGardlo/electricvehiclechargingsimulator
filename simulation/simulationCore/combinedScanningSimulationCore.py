import abc


class CombinedScanningSimulationCore(metaclass=abc.ABCMeta):
    """
    Class represents a general simulation core for activity scanning method
    """

    def __init__(self, time_step):
        self.__simulation_observers = []
        self._time_step = time_step
        self._event_simulation_core = None

    @property
    def event_simulation_core(self):
        return self._event_simulation_core

    @event_simulation_core.setter
    def event_simulation_core(self, value):
        self._event_simulation_core = value

    def register_simulation_listener(self, simulation_observer):
        self.__simulation_observers.append(simulation_observer)

    def remove_simulation_listener(self, simulation_observer):
        self.__simulation_observers.remove(simulation_observer)

    def run_for_time(self, run_from_time, time_to_run):
        """
        Simulation core runs for specified time, it
        :param run_from_time: simulation time in which action starts
        :param time_to_run: time interval to execute actions in
        :return: None
        """
        time_elapsed = 0
        should_continue = self.should_continue_with_activity()
        simulation_time = run_from_time
        while time_elapsed < time_to_run and should_continue:
            current_time_step = min(self._time_step, time_to_run - time_elapsed)
            time_elapsed += current_time_step
            simulation_time += current_time_step
            self.__time_step(simulation_time, current_time_step)
            should_continue = self.should_continue_with_activity()

    def __time_step(self, simulation_time, time_step):
        self.__notify_time_step(simulation_time)
        time_step_successful = self.on_time_step(simulation_time, time_step)
        if not time_step_successful[0]:
            self.__notify_time_step_failure(simulation_time, time_step_successful[1])

    @abc.abstractclassmethod
    def on_time_step(self, simulation_time, time_step):
        """
        Defines action to be performed after each time step of the simulation
        :param simulation_time: current simulation time
        :param time_step: time step of simulation core
        :return:
        """
        pass

    @abc.abstractclassmethod
    def should_continue_with_activity(self):
        """
        This method is used when using this simulation core together with event simulation core
        :return: true if scanning should continue, false if
         there was an event planned in event simulation core
        """
        pass

    @abc.abstractmethod
    def on_replication_end(self):
        """
        Defines action to be perfomed when event core notifies this that replication is over
        :return: None
        """
        pass

    def __notify_time_step(self, simulation_time):
        for observer in self.__simulation_observers:
            observer.on_time_step(self, simulation_time)

    def __notify_time_step_failure(self, simulation_time, failure_info):
        for observer in self.__simulation_observers:
            observer.on_time_step_failure(simulation_time, failure_info)
        raise ValueError('Optimization failed!')
