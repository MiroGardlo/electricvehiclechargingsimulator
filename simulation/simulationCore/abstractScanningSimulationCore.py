import abc


class SimulationCore(metaclass=abc.ABCMeta):
    """
    Class represents a general simulation core for activity scanning method
    """

    def __init__(self, time_step):
        self._current_time = 0
        self._time_step = time_step

    def run(self, number_of_replications, simulation_time):
        """
        Performs simulation
        :param number_of_replications: number of replications
        :param simulation_time: time to simulate
        :return: None
        """
        self.before_simulation_start()
        for i in range(number_of_replications):
            self.__run(simulation_time)
        self.on_simulation_end()

    def __run(self, total_simulation_time):
        self.before_replication_start()
        self._current_time = 0
        while self._current_time <= total_simulation_time and self.should_simulation_continue():
            self._current_time = self._current_time + self._time_step
            self.on_time_step(self._current_time, self._time_step)
        self.on_replication_end()

    @abc.abstractclassmethod
    def before_simulation_start(self):
        """
        Defines action to be perfomed before starting simulation
        :return:
        """
        pass

    @abc.abstractclassmethod
    def before_replication_start(self):
        """
        Defines action to be performed before start of each replication
        :return:
        """
        pass

    @abc.abstractclassmethod
    def on_replication_end(self):
        """
        Defines action to be performed after replication finished
        :return:
        """
        pass

    @abc.abstractclassmethod
    def on_simulation_end(self):
        """
        Defines action to be performed after simulation finished
        :return:
        """
        pass

    @abc.abstractclassmethod
    def on_time_step(self, simulation_time, time_step):
        """
        Defines action to be performed after each time step of the simulation
        :param simulation_time: current simulation time
        :param time_step: time step of simulation core
        :return:
        """
        pass

    @abc.abstractclassmethod
    def should_simulation_continue(self):
        """
        Defines condition
        :return: true if replication should continue, false otherwise
        """
        pass

