import abc


class SimulationObserver(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def on_time_step(self, scanning_simulation_core, simulation_time):
        pass

    @abc.abstractmethod
    def on_time_step_failure(self, simulation_time, failure_details):
        pass

    @abc.abstractmethod
    def on_replication_end(self, replication_number, event_simulation_core):
        pass

    @abc.abstractmethod
    def on_simulation_end(self, event_simulation_core):
        pass

    @abc.abstractmethod
    def on_simulation_start(self, event_simulation_core):
        pass

    @abc.abstractmethod
    def on_event_occurrence(self, simulation_time, event_info, event_simulation_core):
        pass
