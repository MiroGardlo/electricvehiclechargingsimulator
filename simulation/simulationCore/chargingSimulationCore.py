import abc

from simulation.simulationCore.abstractScanningSimulationCore import SimulationCore


class AbstractChargingSimulationCore(SimulationCore, metaclass=abc.ABCMeta):
    def __init__(self, optimizer, time_step, electric_network):
        """
        Creates new simulation core
        :param optimizer: class that takes care of assigning real powers
        :param time_step: time step that simulation core uses for activity scanning
        :param electric_network: graph to perform simulation with
        """
        super().__init__(time_step)
        self._electric_network = electric_network
        self._optimizer = optimizer
