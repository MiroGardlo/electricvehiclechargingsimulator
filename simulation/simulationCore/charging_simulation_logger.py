import os

from simulation.simulationCore.simulation_logger import SimulationObserver


class ChargingSimulationObserver(SimulationObserver):
    def __init__(self, file_name):
        self._logger = open(os.path.join(os.getcwd(), file_name), 'w')

    def on_time_step(self, scanning_simulation_core, simulation_time):
        pass

    def on_time_step_failure(self, simulation_time, failure_details):
        self._logger.write("Optimization failure at time: %f\n%s\n" % (simulation_time, failure_details))

    def on_event_occurrence(self, simulation_time, event_info, event_simulation_core):
        number_of_cars = event_simulation_core.current_number_of_cars
        self._logger.write("*************************************************************\n")
        self._logger.write("Simulation time: %f\nEvent: %s\nCurrent number of cars: %d\n"
                           % (simulation_time, event_info, number_of_cars))
        self._logger.flush()

    def on_replication_end(self, replication_number, event_simulation_core):
        self._logger.write("Replication %d ended\n" % replication_number)
        self.__print_replication_data(replication_number, event_simulation_core)

    def on_simulation_start(self, event_simulation_core):
        self._logger.write("Simulation started with settings:\n")
        self._logger.write("inter arrival generator seed: %d\n" % event_simulation_core.inter_arrival_time_seed)
        self._logger.write("network node generator seed: %d\n" % event_simulation_core.charger_node_generator_seed)

    def on_simulation_end(self, event_simulation_core):
        self._logger.write("Simulation ended\n")
        self._logger.close()

    @staticmethod
    def __print_replication_data(replication_number, event_simulation_core):
        replication_car_number = open(os.path.join(os.getcwd(), "repl-%d-car_number_history.txt" % replication_number),
                                      'w')
        replication_car_number.write(str(event_simulation_core.car_number_history))
        replication_car_number.close()
