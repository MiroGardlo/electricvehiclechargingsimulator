import os

from simulation.simulationCore.simulation_logger import SimulationObserver


class ReplicationSimulationObserver(SimulationObserver):
    def on_time_step(self, scanning_simulation_core, simulation_time):
        pass

    def on_time_step_failure(self, simulation_time, failure_details):
        pass

    def on_event_occurrence(self, simulation_time, event_info, event_simulation_core):
        pass

    def on_replication_end(self, replication_number, event_simulation_core):
        self.__print_replication_data(replication_number, event_simulation_core)

    def on_simulation_start(self, event_simulation_core):
        pass

    def on_simulation_end(self, event_simulation_core):
        final_statistics_file = open(os.path.join(os.getcwd(), "simulation-statistics.txt"), 'w')
        self.__create_report(event_simulation_core, final_statistics_file)
        cumulative_file = open(os.path.join(os.getcwd(), "simulation-statistics-cumulative.txt"), 'w')
        self.__create_cumulative_report(event_simulation_core, cumulative_file)

    @staticmethod
    def __print_replication_data(replication_number, event_simulation_core):
        replication_statistics_file = open(os.path.join(os.getcwd(), "repl-%d-statistics.txt" % replication_number),
                                           'w')
        replication_statistics_file.write("Simulation statistics after %d replications\n" % replication_number)
        ReplicationSimulationObserver.__create_report(event_simulation_core, replication_statistics_file)

    @staticmethod
    def __write_header_for_mean(simulation_file):
        simulation_file.write("Statistics name,Average value,standard deviation,standard error\n")

    @staticmethod
    def __write_header_for_std_dev_std_err(simulation_file):
        simulation_file.write("Statistics name,Average value,standard deviation,standard error\n")

    @classmethod
    def __create_report(cls, event_simulation_core, simulation_file):
        cls.__write_header_for_mean(simulation_file)
        cls.__write_statistics_value(simulation_file,
                                     "Average number of cars",
                                     event_simulation_core.car_number_simulation_statistics)
        cls.__write_statistics_value(simulation_file,
                                     "Average number of cars with full battery",
                                     event_simulation_core.served_number_of_cars_simulation_statistics)
        cls.__write_statistics_value(simulation_file,
                                     "Average number of cars with not full battery",
                                     event_simulation_core.not_fully_served_number_of_cars_simulation_statistics)
        cls.__write_statistics_value(simulation_file, "Average connected time",
                                     event_simulation_core.average_system_time_simulation_statistics)
        cls.__write_statistics_value(simulation_file,
                                     "Average battery state when leaving network",
                                     event_simulation_core.average_battery_state_when_leaving_simulation_statistics)
        cls.__write_statistics_value(simulation_file, "Average remaining budget",
                                     event_simulation_core.average_remaining_budget_simulation_statistics)
        cls.__write_statistics_value(simulation_file, "Average paid price per unit of energy",
                                     event_simulation_core.average_paid_price_per_unit_of_energy_simulation_statistics)
        cls.__write_statistics_value(simulation_file,
                                     "Average number of vehicles with elapsed T max",
                                     event_simulation_core.average_number_of_cars_left_with_elapsed_t_max)
        cls.__write_statistics_value(simulation_file,
                                     "Average number of vehicles with zero budget",
                                     event_simulation_core.average_number_of_cars_left_with_zero_budget)

        cls.__process_level_based_statistics_mean(simulation_file,
                                                  event_simulation_core.average_battery_state_level_simulation_statistics)
        simulation_file.close()

    @classmethod
    def __create_cumulative_report(cls, event_simulation_core, simulation_file):
        cls.__write_header_for_std_dev_std_err(simulation_file)
        cls.__write_std_dev_std_err(simulation_file,
                                    "Average number of cars",
                                    event_simulation_core.car_number_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file,
                                    "Average number of cars with full battery",
                                    event_simulation_core.served_number_of_cars_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file,
                                    "Average number of cars with not full battery",
                                    event_simulation_core.not_fully_served_number_of_cars_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file, "Average connected time",
                                    event_simulation_core.average_system_time_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file,
                                    "Average battery state when leaving network",
                                    event_simulation_core.average_battery_state_when_leaving_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file, "Average remaining budget",
                                    event_simulation_core.average_remaining_budget_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file, "Average paid price per unit of energy",
                                    event_simulation_core.average_paid_price_per_unit_of_energy_simulation_statistics)
        cls.__write_std_dev_std_err(simulation_file,
                                    "Average number of vehicles with elapsed T max",
                                    event_simulation_core.average_number_of_cars_left_with_elapsed_t_max)
        cls.__write_std_dev_std_err(simulation_file,
                                    "Average number of vehicles with zero budget",
                                    event_simulation_core.average_number_of_cars_left_with_zero_budget)

        cls.__process_level_based_statistics_std_dev_std_err(simulation_file,
                                                             event_simulation_core.average_battery_state_level_simulation_statistics)
        simulation_file.close()

    @staticmethod
    def __write_statistics_value(writer, info, statistics):
        writer.write("%s,%.8f,%.8f,%.8f\n" % (info, statistics.get_mean(),
                                              statistics.get_standard_deviation_for_average_values(),
                                              statistics.get_standard_error_for_average_values()))

    @staticmethod
    def __write_std_dev_std_err(writer, info, statistics):
        writer.write("%s,%.8f,%.8f,%.8f\n" % (info,
                                              statistics.get_cumulative_mean(),
                                              statistics.get_standard_deviation(),
                                              statistics.get_standard_error()))

    @classmethod
    def __process_level_based_statistics_mean(cls, simulation_file, level_based_statistics):
        for i in range(level_based_statistics.number_of_levels):
            current_level = i + 1
            current_level_statistics = level_based_statistics.get_level_statistics(current_level)
            info = "Average battery state of vehicles at level %d" % current_level
            cls.__write_statistics_value(simulation_file, info, current_level_statistics)

    @classmethod
    def __process_level_based_statistics_std_dev_std_err(cls, simulation_file, level_based_statistics):
        for i in range(level_based_statistics.number_of_levels):
            current_level = i + 1
            current_level_statistics = level_based_statistics.get_level_statistics(current_level)
            info = "Average battery state of vehicles at level %d" % current_level
            cls.__write_std_dev_std_err(simulation_file, info, current_level_statistics)
