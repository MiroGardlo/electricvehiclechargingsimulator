from simulation.simulationCore.chargingSimulationCore import AbstractChargingSimulationCore
from simulation.simulationUtils.scalingStrategies.CompositeScalingStrategy import CompositeScalingStrategy

from simulation.simulationUtils.statistics import Statistics


class ScalingExperiment(AbstractChargingSimulationCore):
    def __init__(self, optimizer, time_step, electric_network):
        super().__init__(optimizer, time_step, electric_network)
        self.__failure_statistics = Statistics()
        self.__failed_steps = 0
        self.__scaling_strategies = CompositeScalingStrategy(optimizer, electric_network)

    def on_time_step(self, simulation_time, time_step):
        print('Simulation time', str(simulation_time))
        self.__scaling_strategies.optimize()
        if not self.__scaling_strategies.is_solution_optimal:
            self.__failed_steps += 1
        self.__update_wi(simulation_time, time_step)
        self.__print_partial_report(simulation_time)

    def __print_partial_report(self, simulation_time):
        steps = self._current_time / self._time_step
        if steps % 10 == 0:
            print("Time step: " + str(simulation_time))
            print("Absolute failures: ", str(self.__failed_steps))
            print("Failures: ", str((self.__failed_steps / steps) * 100), "%")

    def before_simulation_start(self):
        print("Simulation started")

    def before_replication_start(self):
        self.__failed_steps = 0

    def on_replication_end(self):
        print("Replication Report:")
        steps = self._current_time / self._time_step
        print("Time steps:", str(steps))
        print("Absolute failures: ", str(self.__failed_steps))
        print("Failures: ", str((self.__failed_steps / steps) * 100), "%")
        self._electric_network.reset_network()
        self.__failure_statistics.add_observation(self.__failed_steps)
        self.__scaling_strategies.reset_strategy()

    def on_simulation_end(self):
        print("Simulation finished")
        self.__print_final_report()

    def should_simulation_continue(self):
        return True

    def __update_wi(self, simulation_time, time_step):
        # assign zero power to all nodes to trigger update in willingness to pay
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            current_node.assign_power(0, simulation_time, time_step)

    def __process_optimization_failure(self, simulation_time):
        print('****************************************************')
        print('Optimization failure at step: ', str(simulation_time))
        self.__print_nodal_wi()
        self.__failed_steps += 1

    def __print_nodal_wi(self):
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            print('Node ', str(i), 'has wi: ', str(current_node.nodal_wi))
        print()

    def __print_solution(self, solution):
        for i in range(self._electric_network.number_of_nodes - 1):
            print('Assigned power: ', solution[i])

    def __print_final_report(self):
        number_of_failures = self.__failure_statistics.get_mean()
        steps = self._current_time / self._time_step
        print('****************************************************')
        print('Final report')
        print('Average number of failures: ', str(number_of_failures))
        print('Failure percentage: ', str((number_of_failures / steps) * 100), "%")
