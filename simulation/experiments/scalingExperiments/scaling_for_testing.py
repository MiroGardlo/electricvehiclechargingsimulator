from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from simulation.simulationCore.chargingSimulationCore import AbstractChargingSimulationCore
from simulation.simulationUtils.scalingStrategies.sum_willingness_to_pay_scaling import \
    SumWillingnessToPayScalingStrategy


class TestScaling(AbstractChargingSimulationCore):
    def __init__(self, optimizer, time_step, electric_network):
        super().__init__(optimizer, time_step, electric_network)
        self.__sum_scaling_strategy = SumWillingnessToPayScalingStrategy(optimizer, electric_network)

    def before_replication_start(self):
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(StaticUserCar(1, 0, 10, 0))

    def on_time_step(self, simulation_time, time_step):
        print("Time step: " + str(simulation_time))
        self.__sum_scaling_strategy.optimize()
        solution = self.__sum_scaling_strategy.solution_vector
        self.__print_solution(solution)

    def before_simulation_start(self):
        pass

    def on_simulation_end(self):
        pass

    def should_simulation_continue(self):
        return True

    def on_replication_end(self):
        pass

    def __print_solution(self, solution):
        for i in range(self._electric_network.number_of_nodes - 1):
            print('Assigned power: ', solution[i])
