from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from model.strategies.experimentalStrategies.randomTestUser import RandomTestUser
from simulation.experiments.scalingExperiments.PiWiDivisionTest import PiWiDivisionTest
from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class PiWiDivisionTestWithFixedRoot(PiWiDivisionTest):
    def __init__(self, optimizer, time_step, electric_network):
        super().__init__(optimizer, time_step, electric_network)

    def before_replication_start(self):
        self._prepare_output_files()
        for i in range(1, self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(RandomTestUser(1, 0, 10, 0, UniformRNG(1, self._current_upper_bound)))
        root_node = self._electric_network.get_node(1)
        root_node.connect_car(StaticUserCar(self.__compute_root_node_wi(), 0, 10, 0))

    def __compute_root_node_wi(self):
        return 1 + (self._current_upper_bound - 1) / 2
