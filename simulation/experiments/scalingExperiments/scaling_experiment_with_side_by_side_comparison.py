from model.strategies.experimentalStrategies.randomLocalUser import RandomUserWithLocalChanges
from simulation.simulationCore.chargingSimulationCore import AbstractChargingSimulationCore
from simulation.simulationUtils.generators.EmpiricRNG import EmpiricRNG
from simulation.simulationUtils.generators.uniformRNG import UniformRNG
from simulation.simulationUtils.scalingStrategies.VoltageScalingStrategy import VoltageScalingStrategy
from simulation.simulationUtils.scalingStrategies.max_willingness_to_pay_strategy import \
    MaxWillingnessToPayScalingStrategy
from simulation.simulationUtils.scalingStrategies.sum_willingness_to_pay_scaling import \
    SumWillingnessToPayScalingStrategy
from simulation.simulationUtils.statistics import Statistics


class ScalingExperimentWithSideBySideComparison(AbstractChargingSimulationCore):
    def __init__(self, optimizer, time_step, electric_network, lower_change, upper_change):
        super().__init__(optimizer, time_step, electric_network)
        self.__sum_failure_statistics = Statistics()
        self.__max_failure_statistics = Statistics()
        self.__norm_voltage_failure_statistics = Statistics()
        self.__system_scaling_failure_statistics = Statistics()
        self.__sum_failure_steps = 0
        self.__max_failure_steps = 0
        self.__norm_voltage_failure_steps = 0
        self.__system_scaling_failure_steps = 0
        self.__sum_scaling_strategy = SumWillingnessToPayScalingStrategy(optimizer, electric_network)
        self.__max_scaling_strategy = MaxWillingnessToPayScalingStrategy(optimizer, electric_network)
        self.__voltage_scaling_strategy = VoltageScalingStrategy(optimizer, electric_network)
        self.__system_scaling_strategy = SumWillingnessToPayScalingStrategy(optimizer, electric_network,
                                                                            track_history=False,
                                                                            scale_optimization_matrices=True)
        self.__lower_change = lower_change
        self.__upper_change = upper_change

    def on_time_step(self, simulation_time, time_step):
        self.__sum_scaling_strategy.optimize()
        self.__max_scaling_strategy.optimize()
        self.__voltage_scaling_strategy.optimize()
        self.__system_scaling_strategy.optimize()
        self.__process_solutions()
        self.__check_rank_violations(simulation_time)
        self.__update_wi(simulation_time, time_step)
        self.__print_partial_report(simulation_time)

    def __print_partial_report(self, simulation_time):
        steps = self._current_time / self._time_step
        if steps % 10 == 0:
            print("Time step: " + str(simulation_time))
            print("Absolute failures sum: ", str(self.__sum_failure_steps))
            print("Absolute failures max: ", str(self.__max_failure_steps))
            print("Absolute failures voltage scaling: ", str(self.__norm_voltage_failure_steps))
            print("Absolute failures system scaling: ", str(self.__system_scaling_failure_steps))

            print("Sum Failures percentage: ", str((self.__sum_failure_steps / steps) * 100), "%")
            print("Max Failures percentage: ", str((self.__max_failure_steps / steps) * 100), "%")
            print("Norm voltage failure percentage: ", str((self.__norm_voltage_failure_steps / steps) * 100), "%")
            print("System scaling failure percentage: ", str((self.__system_scaling_failure_steps / steps) * 100), "%")

    def __process_solutions(self):
        if not self.__sum_scaling_strategy.is_solution_found:
            self.__sum_failure_steps += 1
        if not self.__max_scaling_strategy.is_solution_found:
            self.__max_failure_steps += 1
        if not self.__voltage_scaling_strategy.is_solution_found:
            self.__norm_voltage_failure_steps += 1
        if not self.__system_scaling_strategy.is_solution_optimal:
            self.__system_scaling_failure_steps += 1

    def before_simulation_start(self):
        print("Simulation started")

    def before_replication_start(self):
        self.__sum_failure_steps = 0
        self.__max_failure_steps = 0
        self.__norm_voltage_failure_steps = 0
        self.__system_scaling_failure_steps = 0
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(
                RandomUserWithLocalChanges(1, 0, 10, 0, EmpiricRNG([UniformRNG(0, 1), 0.5],
                                                                   [UniformRNG(1000, 5000), 0.5]), self.__lower_change,
                                           self.__upper_change))

    def on_replication_end(self):
        print("Replication Report:")
        steps = self._current_time / self._time_step
        print("Time steps:", str(steps))
        print("Absolute failures sum: ", str(self.__sum_failure_steps))
        print("Absolute failures max: ", str(self.__max_failure_steps))
        print("Norm voltage failures: ", str(self.__norm_voltage_failure_steps))
        print("System scaling failures: ", str(self.__system_scaling_failure_steps))

        print("Sum Failures percentage: ", str((self.__sum_failure_steps / steps) * 100), "%")
        print("Max Failures percentage: ", str((self.__max_failure_steps / steps) * 100), "%")
        print("Norm voltage failure percentage: ", str((self.__norm_voltage_failure_steps / steps) * 100), "%")
        print("System scaling failure percentage: ", str((self.__system_scaling_failure_steps / steps) * 100), "%")

        self._electric_network.reset_network()
        self.__sum_failure_statistics.add_observation(self.__sum_failure_steps)
        self.__max_failure_statistics.add_observation(self.__max_failure_steps)
        self.__norm_voltage_failure_statistics.add_observation(self.__norm_voltage_failure_steps)

    def on_simulation_end(self):
        self.__print_final_report()

    def should_simulation_continue(self):
        return True

    def __update_wi(self, simulation_time, time_step):
        # assign zero power to all nodes to trigger update in willingness to pay
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            current_node.assign_power(0, simulation_time, time_step)

    def __print_nodal_wi(self):
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            print('Node ', str(i), 'has wi: ', str(current_node.nodal_wi))
        print()

    def __print_solution(self, solution):
        for i in range(self._electric_network.number_of_nodes - 1):
            print('Assigned power: ', solution[i])

    def __print_final_report(self):
        number_of_sum_failures = self.__sum_failure_statistics.get_mean()
        number_of_max_failures = self.__max_failure_statistics.get_mean()
        number_of_voltage_failures = self.__norm_voltage_failure_statistics.get_mean()
        number_of_system_scaling_failures = self.__system_scaling_failure_statistics.get_mean()
        steps = self._current_time / self._time_step
        print('****************************************************')
        print("Scaling experiment finished")
        print('Final report')
        print('Average number of sum failures: ', str(number_of_sum_failures))
        print('Average number of max failures: ', str(number_of_max_failures))
        print('Average number of norm voltage failures: ', str(number_of_voltage_failures))
        print('Average number system scaling failures: ', str(number_of_system_scaling_failures))

        print('Sum failure percentage: ', str((number_of_sum_failures / steps) * 100), "%")
        print('Max failure percentage: ', str((number_of_max_failures / steps) * 100), "%")
        print('Norm voltage failure percentage: ', str((number_of_voltage_failures / steps) * 100), "%")
        print('System scaling failure percentage: ', str((number_of_system_scaling_failures / steps) * 100), "%")

    def __check_rank_violations(self, simulation_time):
        if self.__sum_scaling_strategy.has_rank_one_violation:
            self.__print_rank_violation("Sum", simulation_time)
        if self.__max_scaling_strategy.has_rank_one_violation:
            self.__print_rank_violation("Max", simulation_time)
        if self.__voltage_scaling_strategy.has_rank_one_violation:
            self.__print_rank_violation("Voltage scaling", simulation_time)
        if self.__system_scaling_strategy.has_rank_one_violation:
            self.__print_rank_violation("System scaling", simulation_time)

    @staticmethod
    def __print_rank_violation(scaling_strategy_name, simulation_time):
        print("Rank 1 condition was violated by scaling strategy %s at time %d" % (
            scaling_strategy_name, simulation_time))
