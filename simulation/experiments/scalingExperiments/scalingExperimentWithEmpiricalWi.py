from model.strategies.experimentalStrategies.randomLocalUser import RandomUserWithLocalChanges
from simulation.experiments.scalingExperiments.scalingExperiment import ScalingExperiment
from simulation.simulationUtils.generators.EmpiricRNG import EmpiricRNG
from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class ScalingExperimentWithEmpiricalWi(ScalingExperiment):
    def __init__(self, optimizer, time_step, electric_network, lower_bound, upper_bound):
        super().__init__(optimizer, time_step, electric_network)
        self.__lower_bound = lower_bound
        self.__upper_bound = upper_bound

    def before_replication_start(self):
        super().before_replication_start()
        print('****************************************************')
        print('Replication started')
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(
                RandomUserWithLocalChanges(1, 0, 10, 0, EmpiricRNG([UniformRNG(0, 1), 0.5],
                                                                   [UniformRNG(1000, 5000), 0.5]),
                                           self.__lower_bound,
                                           self.__upper_bound))
