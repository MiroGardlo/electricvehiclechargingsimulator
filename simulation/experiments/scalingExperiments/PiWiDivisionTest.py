from model.strategies.experimentalStrategies.randomTestUser import RandomTestUser
from simulation.simulationCore.chargingSimulationCore import AbstractChargingSimulationCore
from simulation.simulationUtils.generators.uniformRNG import UniformRNG
from simulation.simulationUtils.scalingStrategies.sum_willingness_to_pay_scaling import \
    SumWillingnessToPayScalingStrategy


class PiWiDivisionTest(AbstractChargingSimulationCore):
    def __init__(self, optimizer, time_step, electric_network, start_with_upper_bound=10, multiply_upper_bound_by=10):
        super().__init__(optimizer, time_step, electric_network)
        self._current_upper_bound = start_with_upper_bound
        self.__multiply_upper_bound_by = multiply_upper_bound_by
        self.__first_division_strategy_file = None
        self.__second_division_strategy_file = None
        self.__sum_scaling_strategy = SumWillingnessToPayScalingStrategy(optimizer, electric_network)

    def on_time_step(self, simulation_time, time_step):
        self.__sum_scaling_strategy.optimize()
        if not self.__sum_scaling_strategy.is_solution_found:
            self.__process_optimization_failure(simulation_time)
        elif self.__sum_scaling_strategy.has_rank_one_violation:
            self.__first_division_strategy_file.write('Rank 1 was violated!!!!\n')
            self.__second_division_strategy_file.write('Rank 1 was violated!!!!\n')
        else:
            self.__print_pi_wi_relations()
            self.__update_wi(simulation_time, time_step)

    def before_simulation_start(self):
        print("Simulation started")

    def before_replication_start(self):
        self._prepare_output_files()
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(RandomTestUser(1, 0, 10, 0, UniformRNG(1, self._current_upper_bound)))

    def _prepare_output_files(self):
        self.__first_division_strategy_file = open(("wiGreaterThanWj%d.csv" % self._current_upper_bound), 'w')
        self.__second_division_strategy_file = open(("levelDivision%d.csv" % self._current_upper_bound), 'w')

    def on_replication_end(self):
        self.__first_division_strategy_file.close()
        self.__second_division_strategy_file.close()
        self._electric_network.reset_network()
        self._current_upper_bound *= self.__multiply_upper_bound_by

    def on_simulation_end(self):
        print("Experiment finished")

    def should_simulation_continue(self):
        return True

    def __update_wi(self, simulation_time, time_step):
        # assign zero power to all nodes to trigger update in willingness to pay
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            current_node.assign_power(0, simulation_time, time_step)

    def __process_optimization_failure(self, simulation_time):
        self.__first_division_strategy_file.write("Optimization failure at step: %d\n" % simulation_time)
        self.__second_division_strategy_file.write("Optimization failure at step: %d\n" % simulation_time)
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            print('Node ', str(i), 'has wi: ', str(current_node.nodal_wi))

    def __print_solution(self, solution):
        for i in range(self._electric_network.number_of_nodes - 1):
            print('Assigned power: ', solution[i])

    def __print_pi_wi_relations(self):
        solution = self.__sum_scaling_strategy.solution_vector
        self.print_all_division_where_wi_greater_than_wj(solution)
        self.print_all_where_node_i_level_is_greater_than_node_j_level(solution)

    def print_all_division_where_wi_greater_than_wj(self, solution):
        for i in range(1, self._electric_network.number_of_nodes):
            for j in range(i + 1, self._electric_network.number_of_nodes):
                node_i_wi = self._electric_network.get_node(i).nodal_wi
                node_j_wj = self._electric_network.get_node(j).nodal_wi
                node_i_level = self._electric_network.get_node(i).node_level
                node_j_level = self._electric_network.get_node(j).node_level
                node_i_pi = solution[i - 1]
                node_j_pj = solution[j - 1]
                node_i_in_denominator = node_i_wi > node_j_wj
                self.print_division_info(i, j, node_i_pi, node_i_wi, node_j_pj, node_j_wj, node_i_in_denominator,
                                         node_i_level, node_j_level, self.__first_division_strategy_file)

    def print_all_where_node_i_level_is_greater_than_node_j_level(self, solution):
        for i in range(1, self._electric_network.number_of_nodes):
            for j in range(i + 1, self._electric_network.number_of_nodes):
                node_i_level = self._electric_network.get_node(i).node_level
                node_j_level = self._electric_network.get_node(j).node_level
                node_i_wi = self._electric_network.get_node(i).nodal_wi
                node_j_wj = self._electric_network.get_node(j).nodal_wi
                node_i_pi = solution[i - 1]
                node_j_pj = solution[j - 1]
                node_i_in_denominator = node_i_level < node_j_level
                if node_i_level != node_j_level:
                    self.print_division_info(i, j, node_i_pi, node_i_wi, node_j_pj, node_j_wj, node_i_in_denominator,
                                             node_i_level, node_j_level, self.__second_division_strategy_file)

    @staticmethod
    def print_division_info(node_i_number, node_j_number, node_i_pi, node_i_wi, node_j_pj, node_j_wj,
                            node_i_in_denominator, node_i_level, node_j_level, file):
        if node_i_in_denominator:
            division_w = node_i_wi / node_j_wj
            division_p = node_i_pi / node_j_pj
            file.write("%d,%d,%f,%f,%d,%d,%f,%f,%f,%f\n" % (node_i_number, node_i_level, node_i_wi, node_i_pi,
                                                            node_j_number, node_j_level, node_j_wj, node_j_pj,
                                                            division_w, division_p))
        else:
            division_w = node_j_wj / node_i_wi
            division_p = node_j_pj / node_i_pi
            file.write("%d,%d,%f,%f,%d,%d,%f,%f,%f,%f\n" % (node_j_number, node_j_level, node_j_wj, node_j_pj,
                                                            node_i_number, node_i_level, node_i_wi, node_i_pi,
                                                            division_w, division_p))
