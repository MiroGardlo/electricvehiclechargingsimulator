from model.strategies.experimentalStrategies.incrementalTestUser import IncrementalTestUser
from simulation.simulationCore.chargingSimulationCore import AbstractChargingSimulationCore


class ParametersTest(AbstractChargingSimulationCore):
    def __init__(self, optimizer, time_step, electric_network):
        super().__init__(optimizer, time_step, electric_network)
        self.__current_scale_by_factor = 1

    def before_replication_start(self):
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(IncrementalTestUser(1, 0, 10, 0, 0.5))

    def on_time_step(self, simulation_time, time_step):
        print("Time step: " + str(simulation_time))
        solution = self._optimizer.run_optimization(self.__current_scale_by_factor)[1]
        current_step = 1
        while not solution['status'] == 'optimal':
            self.__process_failure(simulation_time)
            current_step += 1
            self.__current_scale_by_factor *= 2
            solution = self._optimizer.run_optimization(self.__current_scale_by_factor)[1]
        self.__print_solution(solution['x'])
        print('solution took: ', str(current_step), ' steps')
        self.__update_wi(simulation_time, time_step)

    def before_simulation_start(self):
        pass

    def on_simulation_end(self):
        pass

    def should_simulation_continue(self):
        return True

    def on_replication_end(self):
        pass

    def __process_failure(self, simulation_time):
        print('****************************************************')
        print('Failure at step: ' + str(simulation_time))
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            print('Node ', str(i), 'has wi: ', str(current_node.nodal_wi))

    def __update_wi(self, simulation_time, time_step):
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            current_node.assign_power(0, simulation_time, time_step)

    def __print_solution(self, solution):
        for i in range(self._electric_network.number_of_nodes - 1):
            print('Assigned power: ', solution[i])
