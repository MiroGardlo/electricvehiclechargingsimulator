import numpy

from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from simulation.experiments.simulationExperiments.staticUserChargingTest import StaticUserChargingTest


class PositionEffectWithStaticUsersTest(StaticUserChargingTest):
    def before_replication_start(self):
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(StaticUserCar(1, 0, numpy.inf, 0))

    def should_simulation_continue(self):
        return True

    def on_replication_end(self):
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.print_all_cars_to_file()
