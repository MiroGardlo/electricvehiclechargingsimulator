from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from simulation.simulationCore.chargingSimulationCore import AbstractChargingSimulationCore
from simulation.simulationUtils.scalingStrategies.sum_willingness_to_pay_scaling import \
    SumWillingnessToPayScalingStrategy


class StaticUserChargingTest(AbstractChargingSimulationCore):
    def __init__(self, optimizer, time_step, electric_network):
        super().__init__(optimizer, time_step, electric_network)
        self.__run_optimization = True
        self.__current_solution = None
        self.__sum_scaling_strategy = SumWillingnessToPayScalingStrategy(optimizer, electric_network)

    def on_time_step(self, simulation_time, time_step):
        print("Time step: " + str(simulation_time))
        if self.__run_optimization:
            self.__sum_scaling_strategy.optimize()
            if not self.__sum_scaling_strategy.is_solution_optimal:
                self.__process_optimization_failure()
        self.__process_solution(simulation_time, time_step)

    def before_simulation_start(self):
        print("Simulation started")

    def before_replication_start(self):
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(StaticUserCar(1, 0, 10, 0))

    def on_replication_end(self):
        print("Replication finished")

    def on_simulation_end(self):
        print("Simulation finished")

    def should_simulation_continue(self):
        current_node_id = 1
        cars_connected = False
        while current_node_id < self._electric_network.number_of_nodes - 1 and not cars_connected:
            current_node = self._electric_network.get_node(current_node_id)
            if current_node.has_cars_to_charge():
                cars_connected = True
            current_node_id += 1
        return cars_connected

    def __process_solution(self, simulation_time, time_step):
        self.__run_optimization = False
        current_solution = self.__sum_scaling_strategy.solution_vector
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.assign_power(current_solution[i], simulation_time, time_step)
            if current_node.has_cars_to_disconnect():
                print('car disconnected: ', str(simulation_time))
                self.__run_optimization = True
                current_node.disconnect_all_ready_vehicles()

    @staticmethod
    def __process_optimization_failure():
        print('Optimization failure!!!')

    @staticmethod
    def __process_disconnected_cars(disconnected_cars):
        for car in disconnected_cars:
            car.disconnect_from_node()
