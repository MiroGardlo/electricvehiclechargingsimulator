from simulation.events.CarDepartureEvent import CarDepartureEvent
from simulation.simulationCore.combinedScanningChargingSimulationCore import CombinedScanningChargingSimulationCore
from simulation.simulationUtils.scalingStrategies.CompositeScalingStrategy import CompositeScalingStrategy


class ChargingScanningCore(CombinedScanningChargingSimulationCore):
    def __init__(self, time_step, electric_network, optimizer):
        super().__init__(time_step, electric_network, optimizer)
        self.__composite_scaling_strategy = CompositeScalingStrategy(optimizer, electric_network)

    def __has_cars_in_network(self):
        return self._electric_network.connected_cars > 0

    def should_continue_with_activity(self):
        return self.__has_cars_in_network() and self._electric_network.cars_to_leave == 0

    def on_time_step(self, simulation_time, time_step):
        self.__composite_scaling_strategy.optimize()
        if not self.__composite_scaling_strategy.is_solution_optimal:
            return self.__process_optimization_failure()
        else:
            self.__update_battery_capacity(simulation_time, time_step)
            return True, None

    def on_replication_end(self):
        self.__composite_scaling_strategy.reset_strategy()

    def __process_optimization_failure(self):
        return False, self.__print_nodal_wi()

    def __print_nodal_wi(self):
        failure_information = ""
        for i in range(1, self._electric_network.number_of_nodes):
            current_node = self._electric_network.get_node(i)
            failure_information += "Node %d, has wi: %f\n" % (i, current_node.nodal_wi)
        return failure_information

    def __update_battery_capacity(self, simulation_time, time_step):
        solution = self.__composite_scaling_strategy.solution_vector
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.assign_power(solution[i], simulation_time, time_step)
            if current_node.has_cars_to_disconnect():
                cars_to_disconnect = current_node.cars_to_leave_network()
                self.__plan_car_departure(simulation_time, cars_to_disconnect)

    def __plan_car_departure(self, simulation_time, cars_to_disconnect):
        for i in range(cars_to_disconnect.__len__()):
            car = cars_to_disconnect[i]
            # in this case charger node is set to null because the car itself contains information
            # needed to disconnect from the node
            self._event_simulation_core.plan_event(
                CarDepartureEvent(self._event_simulation_core, simulation_time, None, car))
