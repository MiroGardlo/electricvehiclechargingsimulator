import numpy

from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from simulation.experiments.simulationExperiments.staticUserChargingTest import StaticUserChargingTest


class WillingnessToPayEffectWithStaticUsersTest(StaticUserChargingTest):
    def before_replication_start(self):
        current_willingness_to_pay = 1
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.connect_car(StaticUserCar(current_willingness_to_pay, 0, numpy.inf, 0))
            current_willingness_to_pay *= 2

    def should_simulation_continue(self):
        return True

    def on_replication_end(self):
        for i in range(self._electric_network.number_of_nodes - 1):
            current_node = self._electric_network.get_node(i + 1)
            current_node.print_all_cars_to_file()
