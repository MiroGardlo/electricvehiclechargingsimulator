import abc

from model.time_info import TimeInfo
from model.elementHistoryInfo import ElementHistoryInfo
from simulation.simulationCore.eventSimulationCore import EventSimulationCore
from simulation.simulationUtils.generators.exponential_rng import ExponentialRNG
from simulation.simulationUtils.generators.uniformDiscreteRNG import UniformDiscreteRNG
from simulation.simulationUtils.level_based_statistics import LevelBasedStatistics
from simulation.simulationUtils.replication_level_based_statistics import ReplicationLevelBasedStatistics
from simulation.simulationUtils.replication_statistics import ReplicationStatistics
from simulation.simulationUtils.statistics import Statistics
from simulation.simulationUtils.weighted_statistics import WeightedStatistics


class RealChargingExperiment(EventSimulationCore, metaclass=abc.ABCMeta):
    def __init__(self, electric_network, car_strategy_generator, inter_arrival_time, replication_mode,
                 random_mode=True):
        super().__init__()
        self._electric_network = electric_network
        self.__replication_mode = replication_mode
        self.__arrival_time_generator = None
        self.__charger_node_generator = None
        if random_mode:
            self.__arrival_time_generator = ExponentialRNG(inter_arrival_time)
            self.__charger_node_generator = UniformDiscreteRNG(1, electric_network.number_of_nodes)
        self.__car_strategy_generator = car_strategy_generator
        # replication statistics
        self.__current_number_of_cars_in_network = 0
        self.__cars_left_fully_charged = 0
        self.__cars_left_without_full_battery = 0
        self.__cars_left_when_t_max_elapsed = 0
        self.__cars_left_when_budget_spent = 0

        self.__car_number_history = ElementHistoryInfo()
        self.__car_number_statistics = WeightedStatistics()
        self.__average_system_time_statistics = Statistics()
        self.__average_battery_state_when_leaving = Statistics()
        self.__average_battery_state_level_statistics = LevelBasedStatistics(electric_network.level_of_network)
        self.__average_paid_price_per_unit_of_energy = Statistics()
        self.__average_remaining_budget = Statistics()
        # simulation statistics
        self.__car_number_simulation_statistics = Statistics()
        self.__served_number_of_cars_simulation_statistics = Statistics()
        self.__not_fully_served_number_of_cars_simulation_statistics = Statistics()
        self.__average_system_time_simulation_statistics = ReplicationStatistics()
        self.__average_battery_state_when_leaving_simulation_statistics = ReplicationStatistics()
        self.__cars_left_when_budget_spent_simulation_statistics = Statistics()
        self.__cars_left_when_t_max_elapsed_simulation_statistics = Statistics()
        self.__average_paid_price_per_unit_of_energy_simulation_statistics = ReplicationStatistics()
        self.__average_remaining_budget_simulation_statistics = ReplicationStatistics()
        self.__average_battery_state_level_simulation_statistics = ReplicationLevelBasedStatistics(
            electric_network.level_of_network)

    @property
    def car_number_statistics(self):
        return self.__car_number_statistics

    @property
    def current_number_of_cars(self):
        return self.__current_number_of_cars_in_network

    @current_number_of_cars.setter
    def current_number_of_cars(self, value):
        self.__current_number_of_cars_in_network = value

    @property
    def current_number_of_fully_served_cars(self):
        return self.__cars_left_fully_charged

    @current_number_of_fully_served_cars.setter
    def current_number_of_fully_served_cars(self, value):
        self.__cars_left_fully_charged = value

    @property
    def current_number_of_not_fully_served_cars(self):
        return self.__cars_left_without_full_battery

    @current_number_of_not_fully_served_cars.setter
    def current_number_of_not_fully_served_cars(self, value):
        self.__cars_left_without_full_battery = value

    @property
    def current_number_of_cars_left_when_budget_spent(self):
        return self.__cars_left_when_budget_spent

    @current_number_of_cars_left_when_budget_spent.setter
    def current_number_of_cars_left_when_budget_spent(self, value):
        self.__cars_left_when_budget_spent = value

    @property
    def current_number_of_cars_left_when_time_elapsed(self):
        return self.__cars_left_when_t_max_elapsed

    @current_number_of_cars_left_when_time_elapsed.setter
    def current_number_of_cars_left_when_time_elapsed(self, value):
        self.__cars_left_when_t_max_elapsed = value

    @property
    def car_number_history(self):
        return self.__car_number_history

    @property
    def car_number_simulation_statistics(self):
        return self.__car_number_simulation_statistics

    @property
    def served_number_of_cars_simulation_statistics(self):
        return self.__served_number_of_cars_simulation_statistics

    @property
    def not_fully_served_number_of_cars_simulation_statistics(self):
        return self.__not_fully_served_number_of_cars_simulation_statistics

    @property
    def average_system_time_statistics(self):
        return self.__average_system_time_statistics

    @property
    def average_system_time_simulation_statistics(self):
        return self.__average_system_time_simulation_statistics

    @property
    def average_battery_state_when_leaving(self):
        return self.__average_battery_state_when_leaving

    @property
    def average_battery_state_level_statistics(self):
        return self.__average_battery_state_level_statistics

    @property
    def average_paid_price_per_unit_of_energy(self):
        return self.__average_paid_price_per_unit_of_energy

    @property
    def average_remaining_budget(self):
        return self.__average_remaining_budget

    @property
    def average_battery_state_when_leaving_simulation_statistics(self):
        return self.__average_battery_state_when_leaving_simulation_statistics

    @property
    def average_paid_price_per_unit_of_energy_simulation_statistics(self):
        return self.__average_paid_price_per_unit_of_energy_simulation_statistics

    @property
    def average_remaining_budget_simulation_statistics(self):
        return self.__average_remaining_budget_simulation_statistics

    @property
    def average_number_of_cars_left_with_zero_budget(self):
        return self.__cars_left_when_budget_spent_simulation_statistics

    @property
    def average_number_of_cars_left_with_elapsed_t_max(self):
        return self.__cars_left_when_t_max_elapsed_simulation_statistics

    @property
    def average_battery_state_level_simulation_statistics(self):
        return self.__average_battery_state_level_simulation_statistics

    @property
    def inter_arrival_time_seed(self):
        if self.__arrival_time_generator is not None:
            return self.__arrival_time_generator.seed
        return -1

    @property
    def charger_node_generator_seed(self):
        if self.__charger_node_generator is not None:
            return self.__charger_node_generator.seed
        return -1

    @property
    def replication_mode(self):
        return self.__replication_mode

    def get_next_car_arrival(self):
        return self._simulation_time + self.__arrival_time_generator.generate_sample()

    def get_next_charger_node(self):
        return self._electric_network.get_node(self.__charger_node_generator.generate_sample())

    def get_next_car(self):
        return self.__car_strategy_generator.generate_car()

    def before_simulation_start(self):
        pass

    def on_simulation_end(self):
        pass

    def car_connected(self):
        self.__current_number_of_cars_in_network += 1

    def car_disconnected(self):
        self.__current_number_of_cars_in_network -= 1

    def on_replication_end(self):
        # extend statistics to cope with current time
        self.__car_number_statistics.update_observation(self.__current_number_of_cars_in_network, self._simulation_time)
        self.__car_number_simulation_statistics.add_observation(self.__car_number_statistics.get_average())

        self.__served_number_of_cars_simulation_statistics.add_observation(self.__cars_left_fully_charged)
        self.__not_fully_served_number_of_cars_simulation_statistics.add_observation(
            self.__cars_left_without_full_battery)
        self.__cars_left_when_budget_spent_simulation_statistics.add_observation(self.__cars_left_when_budget_spent)
        self.__cars_left_when_t_max_elapsed_simulation_statistics.add_observation(self.__cars_left_when_t_max_elapsed)

        self.__average_system_time_simulation_statistics.add_observation(self.__average_system_time_statistics)
        self.__average_battery_state_when_leaving_simulation_statistics.add_observation(
            self.__average_battery_state_when_leaving)
        self.__average_battery_state_level_simulation_statistics.add_observation(
            self.__average_battery_state_level_statistics)
        self.__average_paid_price_per_unit_of_energy_simulation_statistics.add_observation(
            self.__average_paid_price_per_unit_of_energy)
        self.__average_remaining_budget_simulation_statistics.add_observation(
            self.__average_remaining_budget)
        if not self.__replication_mode:
            self._electric_network.print_all_vehicles()
        self._electric_network.reset_network()

    @abc.abstractclassmethod
    def prepare_replication_start_events(self):
        """
        Defines action to be performed after replication finished
        :return:
        """
        pass

    def before_replication_start(self):
        # reset statistics
        self.__current_number_of_cars_in_network = 0
        self.__cars_left_without_full_battery = 0
        self.__cars_left_fully_charged = 0
        self.__cars_left_when_t_max_elapsed = 0
        self.__cars_left_when_budget_spent = 0

        self.__car_number_history.reset_history()
        self.__car_number_history.add_element_time_info(
            TimeInfo(self._simulation_time, self.__current_number_of_cars_in_network))
        self.__car_number_statistics.reset_observation()
        self.__average_system_time_statistics.clear()
        self.__average_battery_state_when_leaving.clear()
        self.__average_paid_price_per_unit_of_energy.clear()
        self.__average_remaining_budget.clear()
        self.__average_battery_state_level_statistics.clear()
        self.prepare_replication_start_events()
