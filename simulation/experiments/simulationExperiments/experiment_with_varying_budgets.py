from model.strategies.chargingStrategies.budgetBasedStrategies.budget_based_user_car import BudgetBasedUserCar
from simulation.events.car_arrival_without_next_arrival import SimpleCarArrivalEvent
from simulation.experiments.simulationExperiments.realChargingExperiment import RealChargingExperiment


class ExperimentWithVariousBudgets(RealChargingExperiment):
    def __init__(self, electric_network, car_strategy_generator):
        super().__init__(electric_network, car_strategy_generator, None, False,
                         random_mode=False)

    def prepare_replication_start_events(self):
        number_of_nodes = self._electric_network.number_of_nodes
        # node 1 will serve as resource that other vehicles share
        current_budget = 500
        for i in range(2, number_of_nodes):
            current_node = self._electric_network.get_node(i)
            car = self.get_next_car()
            ExperimentWithVariousBudgets.check_generated_car(car)
            car.set_maximum_budget(current_budget)
            current_budget *= 1.3
            self.plan_event(SimpleCarArrivalEvent(self, 0, current_node, car))

    @staticmethod
    def check_generated_car(car):
        if not isinstance(car, BudgetBasedUserCar):
            raise ValueError("Budget variation experiment cannot be performed with non budget based vehicle")
