from simulation.events.carArrivalEvent import CarArrivalEvent
from simulation.experiments.simulationExperiments.realChargingExperiment import RealChargingExperiment


class ExperimentWithRandomArrivals(RealChargingExperiment):
    def __init__(self, electric_network, car_strategy_generator, inter_arrival_time, replication_mode):
        super().__init__(electric_network, car_strategy_generator, inter_arrival_time, replication_mode,
                         random_mode=True)

    def prepare_replication_start_events(self):
        self.plan_event(
            CarArrivalEvent(self, self.get_next_car_arrival(), self.get_next_charger_node(), self.get_next_car()))
