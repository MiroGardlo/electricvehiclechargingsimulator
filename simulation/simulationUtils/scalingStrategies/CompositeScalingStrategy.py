"""
This class contains multiple solution finding strategies
that are called right after each other in case one of them fails
"""
from simulation.simulationUtils.scalingStrategies.ScalingStrategy import AbstractScalingStrategy
from simulation.simulationUtils.scalingStrategies.VoltageScalingStrategy import VoltageScalingStrategy
from simulation.simulationUtils.scalingStrategies.max_willingness_to_pay_strategy import \
    MaxWillingnessToPayScalingStrategy
from simulation.simulationUtils.scalingStrategies.sum_willingness_to_pay_scaling import \
    SumWillingnessToPayScalingStrategy


class CompositeScalingStrategy(AbstractScalingStrategy):
    def __init__(self, optimizer, electric_network):
        super().__init__(optimizer)
        self.__scaling_strategies = [SumWillingnessToPayScalingStrategy(optimizer, electric_network),
                                     MaxWillingnessToPayScalingStrategy(optimizer, electric_network),
                                     VoltageScalingStrategy(optimizer, electric_network, 2, 20),
                                     SumWillingnessToPayScalingStrategy(optimizer, electric_network,
                                                                        track_history=False,
                                                                        scale_optimization_matrices=True)]

    def _run_optimization(self):
        optimal_solution_found = False
        current_strategy_index = 0
        solution = None
        while not optimal_solution_found and current_strategy_index < self.__scaling_strategies.__len__():
            current_strategy = self.__scaling_strategies[current_strategy_index]
            current_strategy.optimize()
            if current_strategy.is_solution_optimal:
                optimal_solution_found = True
                solution = current_strategy.solution
            current_strategy_index += 1
        return solution

    def reset_strategy(self):
        for strategy in self.__scaling_strategies:
            strategy.reset_strategy()
