import abc

from simulation.simulationUtils.scalingStrategies.ScalingStrategy import AbstractScalingStrategy


class AbstractWillingnessToPayDivisionStrategy(AbstractScalingStrategy, metaclass=abc.ABCMeta):
    def __init__(self, optimizer, electric_network, track_history=False, scale_optimization_matrices=False):
        super().__init__(optimizer)
        self._electric_network = electric_network
        self.__previous_success_values = []
        self.__track_history = track_history
        self.__scale_optimization_matrices = scale_optimization_matrices

    def _run_optimization(self):
        solution = self._optimizer.run_optimization(divide_wi_by=self._scale_willingness_to_pay_by(),
                                                    rescale_optimization_matrices=self.__scale_optimization_matrices)
        if not (solution[1]['status'] == 'optimal' or solution[0]) and self.__track_history:
            self.__try_using_previous_value()
        else:
            if self.__track_history:
                self.__previous_success_values.insert(0, self._scale_willingness_to_pay_by())
        return solution

    def __try_using_previous_value(self):
        optimization_success = False
        index = 0
        while not optimization_success and index < self.__previous_success_values.__len__():
            solution = self._optimizer.run_optimization(self.__previous_success_values[index])
            if solution[1]['status'] == 'optimal' and not solution[0]:
                optimization_success = True
            index += 0

    def reset_strategy(self):
        self.__previous_success_values.clear()

    @abc.abstractmethod
    def _scale_willingness_to_pay_by(self):
        pass
