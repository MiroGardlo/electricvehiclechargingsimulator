import abc


class AbstractScalingStrategy(metaclass=abc.ABCMeta):
    def __init__(self, optimizer):
        self._optimizer = optimizer
        self._solution = None

    @property
    def solution(self):
        return self._solution

    @property
    def solution_vector(self):
        return self.solution[1]['x']

    @property
    def is_solution_optimal(self):
        if self._solution is not None:
            return self.is_solution_found and not self.has_rank_one_violation
        return False

    @property
    def has_rank_one_violation(self):
        if self._solution is not None:
            return self._solution[0]

    @property
    def is_solution_found(self):
        return self._solution[1]['status'] == 'optimal'

    @abc.abstractclassmethod
    def _run_optimization(self):
        pass

    @abc.abstractmethod
    def reset_strategy(self):
        pass

    def optimize(self):
        self._solution = self._run_optimization()
