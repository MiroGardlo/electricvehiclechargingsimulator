from simulation.simulationUtils.scalingStrategies.ScalingStrategy import AbstractScalingStrategy


class VoltageScalingStrategy(AbstractScalingStrategy):
    def __init__(self, optimizer, electric_network, initial_scaling_factor=2, maximum_scaling_factor=20):
        super().__init__(optimizer)
        self.__electric_network = electric_network
        self.__initial_scaling_factor = initial_scaling_factor
        self.__maximum_scaling_factor = maximum_scaling_factor

    def _run_optimization(self):
        current_scaling_factor = self.__initial_scaling_factor
        solution = self._optimizer.run_optimization(divide_wi_by=self.__electric_network.nodal_wi_sum,
                                                    norm_voltage_scaling=current_scaling_factor)
        while not solution[1]['status'] == 'optimal' or solution[0] \
                and current_scaling_factor <= self.__maximum_scaling_factor:
            current_scaling_factor += 1
            solution = self._optimizer.run_optimization(norm_voltage_scaling=current_scaling_factor)
        self.__rescale_solution(solution, current_scaling_factor)
        return solution

    def reset_strategy(self):
        pass

    @staticmethod
    def __rescale_solution(solution, scaling_factor):
        solution[1]['x'] /= scaling_factor ** 2
