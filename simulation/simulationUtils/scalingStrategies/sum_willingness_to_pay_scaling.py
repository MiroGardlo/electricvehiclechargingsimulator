from simulation.simulationUtils.scalingStrategies.willingnessToPayDivisionScaling import \
    AbstractWillingnessToPayDivisionStrategy


class SumWillingnessToPayScalingStrategy(AbstractWillingnessToPayDivisionStrategy):
    def _scale_willingness_to_pay_by(self):
        return self._electric_network.nodal_wi_sum
