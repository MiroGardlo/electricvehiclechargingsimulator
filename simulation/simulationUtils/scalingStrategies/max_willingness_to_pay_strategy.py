from simulation.simulationUtils.scalingStrategies.willingnessToPayDivisionScaling import \
    AbstractWillingnessToPayDivisionStrategy


class MaxWillingnessToPayScalingStrategy(AbstractWillingnessToPayDivisionStrategy):
    def _scale_willingness_to_pay_by(self):
        return self._electric_network.max_nodal_wi
