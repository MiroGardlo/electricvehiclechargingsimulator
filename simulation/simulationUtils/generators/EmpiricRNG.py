from simulation.simulationUtils.generators.abstractRNG import AbstractRNG
from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class EmpiricRNG(AbstractRNG):
    def __init__(self, *generator_tuple):
        self.__check_values(*generator_tuple)
        self.__generators = generator_tuple
        self.__interval_generator = UniformRNG(0, 1)

    @property
    def generator_size(self):
        return self.__generators.__len__()

    def generate_sample(self):
        interval = self.__interval_generator.generate_sample()
        interval_found = False
        # retrieve probability form first tuple
        generator_index = 0
        current_generator = self.__generators[generator_index][1]
        while not interval_found:
            if interval < current_generator:
                interval_found = True
            else:
                generator_index += 1
                current_generator += self.__generators[generator_index][1]
        return self.__generators[generator_index][0].generate_sample()

    @staticmethod
    def __check_values(*generators):
        check_next = True
        current_sum = 0
        current_generator_index = 0
        while check_next and current_generator_index < generators.__len__():
            current_sum += generators[current_generator_index][1]
            current_generator_index += 1
            if current_sum >= 1:
                check_next = False
        current_sum_difference = abs(1 - current_sum)
        if current_generator_index != generators.__len__() or current_sum_difference > 1e-8:
            raise ValueError('Sum of all probabilities must be 1.0!')
