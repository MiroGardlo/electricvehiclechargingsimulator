from random import Random

from simulation.simulationUtils.generators.abstractRNG import AbstractRNG


class ExponentialRNG(AbstractRNG):
    def __init__(self, inter_arrival_time):
        self.__inter_arrival_time = inter_arrival_time
        self.__exponential_generator = Random()
        self._init_seed()
        self.__exponential_generator.seed(self._seed)

    def generate_sample(self):
        return self.__exponential_generator.expovariate(1 / self.__inter_arrival_time)
