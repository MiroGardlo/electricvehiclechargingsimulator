from model.strategies.chargingStrategies.FlexibleUserCar import FlexibleUserCar
from model.strategies.chargingStrategies.budgetBasedStrategies.affordable_price_spender import AffordablePriceSpender
from model.strategies.chargingStrategies.budgetBasedStrategies.combined_affordable_money_spender import \
    CombinedAffordableMoneySpender
from model.strategies.chargingStrategies.budgetBasedStrategies.constant_energy_gainer_user import \
    ConstantEnergyGainerUser
from model.strategies.chargingStrategies.budgetBasedStrategies.constant_per_time_spender import ConstantPerTimeSpender
from model.strategies.chargingStrategies.inflexibleUserCar import InflexibleUserCar
from model.strategies.chargingStrategies.staticUserCar import StaticUserCar


class StrategiesLambdaResolver:
    def __init__(self, time_step):
        self.__strategies_dictionary = {
            "static_user": lambda: StaticUserCar(1, 0, 10, 0),
            "static_user_2": lambda: StaticUserCar(1, 0, 20, 0),
            "static_user_limited_time": lambda: StaticUserCar(1, 0, 20, 0, 500),
            "inflexible_user": lambda: InflexibleUserCar(1, 0, 10, 0, 500, 1),
            "inflexible_user_2": lambda: InflexibleUserCar(1, 0, 20, 0, 500, 1),
            "flexible_user": lambda: FlexibleUserCar(1, 0, 10, 0, 500, 1000),
            "flexible_user_2": lambda: FlexibleUserCar(1, 0, 20, 0, 500, 1000),
            # budget based strategies
            "affordable_price_spender": lambda: AffordablePriceSpender(1, 0, 20, 0, 1000, 500, time_step, 0.001, 0.01),
            "affordable_price_spender_2": lambda: AffordablePriceSpender(1, 0, 20, 0, 500, 500, time_step, 0.001, 0.01),
            "constant_spender_1": lambda: ConstantPerTimeSpender(0, 20, 0, 500, 500),
            "constant_spender_2": lambda: ConstantPerTimeSpender(0, 20, 0, 1000, 500),
            "constant_spender_3": lambda: ConstantPerTimeSpender(0, 20, 0, 1500, 500),
            "constant_spender_4": lambda: ConstantPerTimeSpender(0, 20, 0, 2000, 500),
            "constant_energy_gainer": lambda: ConstantEnergyGainerUser(1, 0, 20, 0, 1000, 500, time_step, 1),
            "combined": lambda: CombinedAffordableMoneySpender(1, 0, 20, 0, 1000, 500, time_step, 0.001, 0.01, 0.75),
            "combined_2": lambda: CombinedAffordableMoneySpender(1, 0, 20, 0, 1000, 500, time_step, 0.001, 0.01, 0.5)
        }

    def resolve_strategy(self, strategy_name):
        return self.__strategies_dictionary[strategy_name]
