import numpy

from simulation.simulationUtils.generators.EmpiricRNG import EmpiricRNG
from simulation.simulationUtils.generators.deterministic_rng import DeterministicRNG


class CarTypeGenerator:
    def __init__(self, empiric_car_generator, car_strategies_array):
        if empiric_car_generator is not None and car_strategies_array is not None:
            self.__check_size(empiric_car_generator, car_strategies_array)
        self.__strategy_generator = empiric_car_generator
        self.__possible_strategies = car_strategies_array

    def generate_car(self):
        strategy_index = self.__strategy_generator.generate_sample()
        # call method that generates the car
        return self.__possible_strategies[strategy_index]()

    def init_from_file(self, file_name, lambda_resolver):
        generators = []
        strategies = []
        car_strategies = numpy.atleast_1d(numpy.genfromtxt(file_name, delimiter=',', dtype=None))
        generator_index = 0
        for strategy in car_strategies:
            probability = strategy[1]
            generators.append([DeterministicRNG(generator_index), probability])
            strategies.append(lambda_resolver.resolve_strategy(strategy[0].decode("utf-8")))
            generator_index += 1
        self.__possible_strategies = strategies
        # sum check is not performed when reading from file
        self.__strategy_generator = EmpiricRNG(*generators)

    @staticmethod
    def __check_size(empiric_car_generator, car_strategies_array):
        if empiric_car_generator.generator_size != car_strategies_array.__len__():
            raise ValueError('Generator size and car strategies sizes must be equal!')
