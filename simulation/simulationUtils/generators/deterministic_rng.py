from simulation.simulationUtils.generators.abstractRNG import AbstractRNG


class DeterministicRNG(AbstractRNG):
    def __init__(self, return_value):
        self.__return_value = return_value

    def generate_sample(self):
        return self.__return_value
