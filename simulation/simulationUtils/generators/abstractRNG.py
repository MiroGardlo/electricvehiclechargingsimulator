import abc
from random import Random


class AbstractRNG(metaclass=abc.ABCMeta):
    __seed_generator = Random()

    def _init_seed(self):
        self._seed = AbstractRNG.__seed_generator.randint(0, 9223372036854775807)

    @property
    def seed(self):
        return self._seed

    @abc.abstractclassmethod
    def generate_sample(self):
        pass
