from random import Random

from simulation.simulationUtils.generators.abstractRNG import AbstractRNG


class UniformRNG(AbstractRNG):
    def __init__(self, lower_bound, upper_bound):
        self.__init_generator(lower_bound, upper_bound)

    def __init_generator(self, lower_bound, upper_bound):
        self._init_seed()
        self.__lower_bound = lower_bound
        self.__upper_bound = upper_bound
        self.__uniform_rng = Random()
        self.__uniform_rng.seed(self._seed)

    def generate_sample(self):
        return self.__uniform_rng.uniform(self.__lower_bound, self.__upper_bound)
