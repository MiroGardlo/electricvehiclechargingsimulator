from simulation.simulationUtils.statistics import Statistics


class LevelBasedStatistics:
    def __init__(self, number_of_levels):
        self.__create_statistics(number_of_levels)

    @property
    def number_of_levels(self):
        return self.__level_statistics.__len__()

    def get_level_statistics(self, level):
        return self.__level_statistics[level - 1]

    def add_observation(self, level, observation):
        self.get_level_statistics(level).add_observation(observation)

    def get_mean(self, level):
        return self.get_level_statistics(level).get_mean()

    def get_standard_error(self, level):
        return self.get_level_statistics(level).get_standard_error()

    def get_standard_deviation(self, level):
        return self.get_level_statistics(level).get_standard_deviation()

    def clear(self):
        for statistics in self.__level_statistics:
            statistics.clear()

    def __create_statistics(self, number_of_levels):
        self.__level_statistics = []
        for i in range(number_of_levels):
            self.__level_statistics.append(Statistics())
