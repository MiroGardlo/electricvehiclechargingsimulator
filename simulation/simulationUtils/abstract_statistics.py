import abc


class AbstractStatistics(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_mean(self):
        pass

    @abc.abstractmethod
    def get_standard_deviation(self):
        pass

    @abc.abstractmethod
    def get_standard_error(self):
        pass

    @abc.abstractmethod
    def add_observation(self, observation_object):
        pass

    @abc.abstractmethod
    def get_standard_deviation_for_average_values(self):
        pass

    @abc.abstractmethod
    def get_standard_error_for_average_values(self):
        pass

    @abc.abstractmethod
    def get_cumulative_mean(self):
        pass

    @abc.abstractmethod
    def clear(self):
        pass
