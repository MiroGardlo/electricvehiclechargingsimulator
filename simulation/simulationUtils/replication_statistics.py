from simulation.simulationUtils.abstract_statistics import AbstractStatistics
from simulation.simulationUtils.statistics import Statistics


class ReplicationStatistics(AbstractStatistics):
    def __init__(self):
        self.__mean_values_statistics = Statistics()
        self.__summary_statistics = Statistics()

    def add_observation(self, statistics):
        if statistics.has_observation:
            self.__mean_values_statistics.add_observation(statistics.get_mean())
            self.__summary_statistics.update_observation_from_statistics(statistics)

    def get_mean(self):
        return self.__mean_values_statistics.get_mean()

    def get_standard_deviation_for_average_values(self):
        return self.__mean_values_statistics.get_standard_deviation()

    def get_standard_error_for_average_values(self):
        return self.__mean_values_statistics.get_standard_error()

    def get_standard_error(self):
        # mean value of all recorded data
        return self.__summary_statistics.get_standard_error()

    def get_standard_deviation(self):
        # mean value of all recorded data
        return self.__summary_statistics.get_standard_deviation()

    def get_cumulative_mean(self):
        return self.__summary_statistics.get_mean()

    def clear(self):
        self.__mean_values_statistics.clear()
        self.__summary_statistics.clear()
