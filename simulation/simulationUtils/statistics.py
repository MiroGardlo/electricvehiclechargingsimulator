import numpy

from simulation.simulationUtils.abstract_statistics import AbstractStatistics


class Statistics(AbstractStatistics):
    def __init__(self):
        """
        Creates new empty statistics
        """
        self.__init_statistics()

    @property
    def has_observation(self):
        return self.__number_of_observations > 0

    def __init_statistics(self):
        self.__observed_sum = 0
        self.__observed_sum_squared = 0
        self.__min = numpy.inf
        self.__max = -numpy.inf
        self.__number_of_observations = 0

    def clear(self):
        self.__init_statistics()

    def add_observation(self, observation):
        """
        Adds observation to the statistics
        :param observation: observation to add to statistics
        :return: none
        """
        self.__observed_sum += observation
        self.__observed_sum_squared += observation ** 2
        self.__number_of_observations += 1
        self.__check_max_min(observation)

    def update_observation_from_statistics(self, statistics):
        self.__observed_sum += statistics.__observed_sum
        self.__observed_sum_squared += statistics.__observed_sum_squared
        self.__number_of_observations += statistics.__number_of_observations

    def __check_max_min(self, observation):
        if self.__max < observation:
            self.__max = observation
        if self.__min > observation:
            self.__min = observation

    def get_standard_deviation(self):
        if self.__number_of_observations > 0:
            fraction_of_sum_squared = self.__observed_sum_squared / self.__number_of_observations
            fraction_of_sum = self.__observed_sum / self.__number_of_observations
            squared_standard_deviation = fraction_of_sum_squared - numpy.power(fraction_of_sum, 2)
            if abs(squared_standard_deviation) < 1e-8:
                squared_standard_deviation = 0
            return numpy.sqrt(squared_standard_deviation)
        # -1 indicates that we are not able to compute standard
        return -1.0

    def get_standard_error(self):
        if self.__number_of_observations > 0:
            return self.get_standard_deviation() / numpy.sqrt(self.__number_of_observations)
        return -1.0

    def get_mean(self):
        """
        Returns average value of observations
        :return: mean
        """
        if self.__number_of_observations > 0:
            return self.__observed_sum / self.__number_of_observations
        return 0.0

    def get_standard_deviation_for_average_values(self):
        return self.get_standard_deviation()

    def get_standard_error_for_average_values(self):
        return self.get_standard_error()

    def get_cumulative_mean(self):
        return self.get_mean()
