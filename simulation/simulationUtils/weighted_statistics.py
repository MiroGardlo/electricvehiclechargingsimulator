class WeightedStatistics:
    def __init__(self, compute_statistics_from=0):
        self.__compute_statistics_from = compute_statistics_from
        self.__timeOfLastChange = compute_statistics_from
        self.__changeSum = 0
        self.__weightSum = 0

    def update_observation(self, current_size, current_time):
        self.__changeSum += current_size * (current_time - self.__timeOfLastChange)
        self.__weightSum = current_time - self.__compute_statistics_from
        self.__timeOfLastChange = current_time

    def reset_observation(self):
        self.__timeOfLastChange = 0
        self.__compute_statistics_from = 0
        self.__changeSum = 0
        self.__weightSum = 0

    def get_average(self):
        if self.__weightSum > 0:
            return self.__changeSum / self.__weightSum
        return 0
