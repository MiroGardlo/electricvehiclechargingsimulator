from simulation.simulationUtils.replication_statistics import ReplicationStatistics


class ReplicationLevelBasedStatistics:
    def __init__(self, number_of_levels):
        self.__create_statistics(number_of_levels)

    @property
    def number_of_levels(self):
        return self.__level_statistics.__len__()

    def add_observation(self, statistics):
        for i in range(self.number_of_levels):
            self.get_level_statistics(i + 1).add_observation(statistics.get_level_statistics(i + 1))

    def get_mean(self, level):
        return self.get_level_statistics(level).get_mean()

    def get_standard_deviation_for_average_values(self, level):
        return self.get_level_statistics(level).get_standard_deviation_for_average_values()

    def get_standard_error_for_average_values(self, level):
        return self.get_level_statistics(level).get_standard_error_for_average_values()

    def get_level_statistics(self, level):
        return self.__level_statistics[level - 1]

    def get_standard_error(self, level):
        return self.get_level_statistics(level).get_standard_error()

    def get_standard_deviation(self, level):
        return self.get_level_statistics(level).get_standard_deviation()

    def get_cumulative_mean(self, level):
        return self.get_level_statistics(level).get_cumulative_mean()

    def __create_statistics(self, number_of_levels):
        self.__level_statistics = []
        for i in range(number_of_levels):
            self.__level_statistics.append(ReplicationStatistics())
