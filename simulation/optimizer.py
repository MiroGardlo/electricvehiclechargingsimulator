"""
decision variable matrix structure:
Let n = number of nodes:
p1, p2, .... , pn - nodal power: n- 1 , indexes : <0, n-2>
V00, V11, V22, ..., Vnn - nodal voltage value: n, indexes: <n-1, 2n - 2>
Re_Vij - for each edge in the network, size: n - 1, indexes: <2n - 1, 3n-3>
Im_Vij - for each edge in the network, size: n - 1, indexes: <3n-2, 4n-4>
Then number of decision variables : 4n - 3
"""
import cvxopt
import numpy as np


class NetworkOptimizer:
    def __init__(self, electric_network, alpha):
        self.__electric_network = electric_network
        self.alpha = alpha
        norm_voltage = electric_network.norm_voltage
        self.lower_voltage = (norm_voltage * (1 - alpha)) * (norm_voltage * (1 - alpha))
        self.upper_voltage = (norm_voltage * (1 + alpha)) * (norm_voltage * (1 + alpha))
        self.__scale_willingness_to_pay = 1
        self.__absolute_accuracy = 1e-7
        self.__create_inequality_matrix()
        self.__create_equality_matrices()

    @property
    def __number_of_decision_variables(self):
        return 4 * self.__electric_network.number_of_nodes - 3

    @property
    def __re_vij_range(self):
        nodes = self.__electric_network.number_of_nodes
        return [2 * nodes - 1, 3 * nodes - 2]

    @property
    def __im_vij_range(self):
        nodes = self.__electric_network.number_of_nodes
        return [3 * nodes - 2, 4 * nodes - 3]

    @property
    def __nodal_voltage_range(self):
        nodes = self.__electric_network.number_of_nodes
        return [nodes - 1, 2 * nodes - 1]

    @property
    def __nodal_power_range(self):
        nodes = self.__electric_network.number_of_nodes
        return [0, nodes - 1]

    def __create_inequality_matrix(self):
        self.__create_nodal_voltage_inequality_matrix()
        self.__create_socp_inequality_matrix()

    def __create_nodal_voltage_inequality_matrix(self):
        number_of_nodes = self.__electric_network.number_of_nodes
        decision_variables_number = 4 * number_of_nodes - 3
        self.g_voltage_inequality = np.zeros((decision_variables_number, 2 * number_of_nodes))
        self.h_voltage_inequality = np.zeros((2 * number_of_nodes))
        voltage_range = self.__nodal_voltage_range
        for i in range(number_of_nodes):
            # V_lower <= Vii
            self.g_voltage_inequality[voltage_range[0] + i, 2 * i] = -1
            self.h_voltage_inequality[2 * i] = -self.lower_voltage
            # Vii <= V_upper
            self.g_voltage_inequality[voltage_range[0] + i, 2 * i + 1] = 1
            self.h_voltage_inequality[2 * i + 1] = self.upper_voltage

    def __create_socp_inequality_matrix(self):
        number_of_nodes = self.__electric_network.number_of_nodes
        decision_variables_number = 4 * number_of_nodes - 3
        # 4*(number of nodes - 1) => 4 for each edge in graph
        # dims dictionary will be 4 for each edge
        self.g_socp_inequality = np.zeros((decision_variables_number, 4 * (number_of_nodes - 1)))
        self.h_socp_inequality = np.zeros((4 * (number_of_nodes - 1)))
        nodal_voltage_range = self.__nodal_voltage_range
        im_range = self.__im_vij_range
        re_range = self.__re_vij_range
        current_constraint = 0
        for edge_tuple in self.__electric_network:
            # value is not stored as edge object, but as set - retrieve the edge from set
            edge = next(iter(edge_tuple[1]))
            self.g_socp_inequality[nodal_voltage_range[0] + edge.starting_node.node_number, 4 * current_constraint] = -1
            self.g_socp_inequality[nodal_voltage_range[0] + edge.end_node.node_number, 4 * current_constraint] = -1
            self.g_socp_inequality[re_range[0] + edge.edge_number, 4 * current_constraint + 1] = -2
            self.g_socp_inequality[im_range[0] + edge.edge_number, 4 * current_constraint + 2] = -2
            self.g_socp_inequality[
                nodal_voltage_range[0] + edge.starting_node.node_number, 4 * current_constraint + 3] = -1
            self.g_socp_inequality[nodal_voltage_range[0] + edge.end_node.node_number, 4 * current_constraint + 3] = 1
            current_constraint += 1

    def __create_equality_matrices(self):
        self.__create_power_equalities()
        self.__create_constant_equality_matrix()
        self.__zero_power_constraints()

    def __create_power_equalities(self):
        # do this for all nodes except root - root has id equal to 0
        number_of_nodes = self.__electric_network.number_of_nodes
        decision_variables_number = 4 * number_of_nodes - 3
        self.real_power_equalities = np.zeros((decision_variables_number, number_of_nodes - 1))
        self.reactive_power_equalities = np.zeros((decision_variables_number, number_of_nodes - 1))
        real_voltage_range = self.__re_vij_range
        imaginary_voltage_range = self.__im_vij_range
        nodal_voltage_range = self.__nodal_voltage_range
        for i in range(1, number_of_nodes):
            current_node = self.__electric_network.get_node(i)
            self.real_power_equalities[i - 1, i - 1] = 1
            current_conductance_constant = 0
            current_susceptance_constant = 0
            for j in range(current_node.node_degree):
                neighbour = current_node.get_neighbour(j)
                current_edge = neighbour.edge_properties
                positive_imaginary_constants = i < neighbour.node.node_number
                current_conductance_constant = current_conductance_constant + current_edge.conductance
                current_susceptance_constant = current_susceptance_constant - current_edge.susceptance
                self.__process_edge_real_power(current_edge, current_conductance_constant, i, imaginary_voltage_range,
                                               nodal_voltage_range, real_voltage_range, positive_imaginary_constants)
                self.__process_edge_reactive_power(current_edge, current_susceptance_constant, i,
                                                   imaginary_voltage_range,
                                                   nodal_voltage_range, real_voltage_range,
                                                   positive_imaginary_constants)

    def __process_edge_real_power(self, current_edge, current_conductance_constant, i, imaginary_voltage_range,
                                  nodal_voltage_range, real_voltage_range, positive_imaginary_constants):
        susceptance_constant = current_edge.susceptance
        if not positive_imaginary_constants:
            susceptance_constant *= -1
        self.real_power_equalities[
            real_voltage_range[0] + current_edge.edge_number, i - 1] = -current_edge.conductance
        self.real_power_equalities[
            imaginary_voltage_range[0] + current_edge.edge_number, i - 1] = susceptance_constant
        self.real_power_equalities[nodal_voltage_range[0] + i, i - 1] = current_conductance_constant

    def __process_edge_reactive_power(self, current_edge, current_susceptance_constant, node_id,
                                      imaginary_voltage_range,
                                      nodal_voltage_range, real_voltage_range, positive_imaginary_constants):
        conductance_constant = current_edge.conductance
        if not positive_imaginary_constants:
            conductance_constant *= -1
        self.reactive_power_equalities[
            real_voltage_range[0] + current_edge.edge_number, node_id - 1] = current_edge.susceptance
        self.reactive_power_equalities[
            imaginary_voltage_range[0] + current_edge.edge_number, node_id - 1] = conductance_constant
        self.reactive_power_equalities[nodal_voltage_range[0] + node_id, node_id - 1] = current_susceptance_constant

    def __create_constant_equality_matrix(self):
        self.equality_constants_matrix = np.zeros((1, 2 * (self.__electric_network.number_of_nodes - 1)))

    def __zero_power_constraints(self):
        zero_constraints = []
        number_of_nodes = self.__electric_network.number_of_nodes
        decision_variables_number = 4 * number_of_nodes - 3
        for i in range(1, self.__electric_network.number_of_nodes):
            current_node = self.__electric_network.get_node(i)
            if not current_node.has_cars_to_charge():
                zero_equality = np.zeros((decision_variables_number, 1))
                zero_equality[current_node.node_number - 1, 0] = 1
                zero_constraints.append(zero_equality)
        if len(zero_constraints) > 0:
            not_connected_constraints = np.column_stack(zero_constraints)
            zero_constraints_constants = np.zeros((1, zero_constraints.__len__()))
            return not_connected_constraints, zero_constraints_constants
        return None

    def run_optimization(self, divide_wi_by=1, norm_voltage_scaling=1, absolute_accuracy=1e-7,
                         rescale_optimization_matrices=False):
        self.__scale_willingness_to_pay = divide_wi_by
        if self.__scale_willingness_to_pay < 0:
            raise ValueError(('Cannot scale by wi by negative number %.20f' % (self.__scale_willingness_to_pay)))
        self.__absolute_accuracy = absolute_accuracy
        number_of_nodes = self.__electric_network.number_of_nodes
        G = cvxopt.matrix(np.hstack((self.g_voltage_inequality, self.g_socp_inequality)).T)
        h = cvxopt.matrix(np.hstack((norm_voltage_scaling ** 2 * self.h_voltage_inequality, self.h_socp_inequality)).T)
        not_connected_constraints = self.__zero_power_constraints()
        if not_connected_constraints is not None:
            A = cvxopt.matrix(
                np.hstack((self.real_power_equalities, self.reactive_power_equalities, not_connected_constraints[0])).T)
            b = cvxopt.matrix(np.hstack((self.equality_constants_matrix, not_connected_constraints[1])).T)
        else:
            A = cvxopt.matrix(np.hstack((self.real_power_equalities, self.reactive_power_equalities)).T)
            b = cvxopt.matrix(np.hstack(self.equality_constants_matrix).T)

            # change l in case of adding nonnegative constraints
        dims = {'l': 2 * number_of_nodes, 'q': [4] * (number_of_nodes - 1), 's': [0]}
        cvxopt.solvers.options['maxiters'] = 10000
        cvxopt.solvers.options['show_progress'] = False
        cvxopt.solvers.options['abstol'] = absolute_accuracy

        if rescale_optimization_matrices:
            a_scaling_factor = self.__find_scaling_factor(A)
            solution = cvxopt.solvers.cp(self.__optimize, A=1 / a_scaling_factor * A, b=1 / a_scaling_factor * b,
                                         G=G, h=h,
                                         dims=dims)
        else:
            solution = cvxopt.solvers.cp(self.__optimize, A=A, b=b, G=G, h=h, dims=dims)

        rank_violated = self.__check_rank(solution['x'])
        return rank_violated, solution

    def __check_rank(self, solution):
        real_voltage_range = self.__re_vij_range
        imaginary_voltage_range = self.__im_vij_range
        nodal_voltage_range = self.__nodal_voltage_range
        number_of_nodes = self.__electric_network.number_of_nodes
        rank_1_violation = False
        current_node = 0
        while not rank_1_violation and current_node < number_of_nodes:
            starting_node = self.__electric_network.get_node(current_node)
            for i in range(starting_node.node_degree):
                neighbour = starting_node.get_neighbour(i).node
                neighbour_id = neighbour.node_number
                edge_id = starting_node.edge_number_of_neighbour(neighbour_id)
                if neighbour_id > starting_node.node_number:
                    vij = complex(solution[real_voltage_range[0] + edge_id],
                                  solution[imaginary_voltage_range[0] + edge_id])
                    vji = vij.conjugate()
                    rank_1_violation = abs((vij * vji) / (solution[nodal_voltage_range[0] + current_node] * solution[
                        nodal_voltage_range[0] + neighbour_id])) - 1 > self.__absolute_accuracy
                else:
                    vij = complex(solution[real_voltage_range[0] + edge_id],
                                  -solution[imaginary_voltage_range[0] + edge_id])
                    vji = vij.conjugate()
                    rank_1_violation = abs((vij * vji) / (solution[nodal_voltage_range[0] + current_node] * solution[
                        nodal_voltage_range[0] + neighbour_id])) - 1 > self.__absolute_accuracy
            current_node += 1
        return rank_1_violation

    def __optimize(self, x=None, z=None):
        if x is None:
            return self.__create_initial_solution()
        objective_with_derivative = self.__calculate_f_df(x)
        if objective_with_derivative is None:
            return None
        if z is None:
            return objective_with_derivative
        h = self.__calculate_h(x, z)
        return objective_with_derivative[0], objective_with_derivative[1], h

    def __create_initial_solution(self):
        # initial solution that is in the domain of objective function
        number_of_nodes = self.__electric_network.number_of_nodes
        initial_solution = np.zeros((4 * number_of_nodes - 3, 1))
        for i in range(number_of_nodes - 1):
            # pi is stored on position id - 1, retrieve node with id = i + 1
            node = self.__electric_network.get_node(i + 1)
            if node.nodal_wi > 0:
                initial_solution[i, 0] = 1
        return 0, cvxopt.matrix(initial_solution)

    def __calculate_f_df(self, x):
        # objective function and first derivative
        objective_function = 0
        number_of_nodes = self.__electric_network.number_of_nodes
        DF = np.zeros((1, (4 * number_of_nodes - 3)))
        feasible = True
        x_index = 0
        while feasible and x_index < number_of_nodes - 1:
            current_node = self.__electric_network.get_node(x_index + 1)
            if current_node.nodal_wi > 0 and x[x_index, 0] > 0:
                objective_function -= (current_node.nodal_wi / self.__scale_willingness_to_pay) * cvxopt.log(
                    x[x_index, 0])
                DF[0, x_index] = (-current_node.nodal_wi / self.__scale_willingness_to_pay) / x[x_index, 0]
            elif current_node.nodal_wi > 0 >= x[x_index, 0]:
                feasible = False
            x_index += 1
        if feasible:
            return objective_function, cvxopt.matrix(DF)
        return None

    def __calculate_h(self, x, z):
        number_of_nodes = self.__electric_network.number_of_nodes
        h = cvxopt.matrix(np.zeros(((4 * number_of_nodes - 3), (4 * number_of_nodes - 3))))
        for i in range(number_of_nodes - 1):
            # pi is stored on position id - 1, retrieve node with id = i + 1
            current_node = self.__electric_network.get_node(i + 1)
            if current_node.nodal_wi > 0:
                h[i, i] = (current_node.nodal_wi / self.__scale_willingness_to_pay) * x[i, 0] ** -2
        return z[0] * h

    @staticmethod
    def __find_scaling_factor(matrix):
        shape = matrix.size
        rows = shape[0]
        columns = shape[1]
        max_row_sum = 0
        for i in range(rows):
            row_sum = 0
            for j in range(columns):
                row_sum += abs(matrix[[i], [j]][0])
            if row_sum > max_row_sum:
                max_row_sum = row_sum
        return max_row_sum
