import abc

from simulation.events.event import SimulationEvent


class CarChargingEvent(SimulationEvent, metaclass=abc.ABCMeta):
    """
    Base class for all events associated with car charging
    """
    def __init__(self, simulation_core, event_time, network_node, car, event_detail = None):
        super().__init__(simulation_core, event_time, event_detail)
        self._charger_node = network_node
        self._car = car
