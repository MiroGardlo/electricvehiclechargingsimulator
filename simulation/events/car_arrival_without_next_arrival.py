from model.time_info import TimeInfo
from simulation.events.CarChargingEvent import CarChargingEvent

from simulation.events.latest_car_departure import LatestCarDepartureEvent


class SimpleCarArrivalEvent(CarChargingEvent):
    def __init__(self, simulation_core, event_time, network_node, car):
        super().__init__(simulation_core, event_time, network_node, car)

    def execute_action(self):
        self._event_detail = ("Car with id: %d is entering network at: %f, node: %d"
                              % (self._car.car_id, self._event_time, self._charger_node.node_number))
        self._simulation_core.car_number_statistics.update_observation(self._simulation_core.current_number_of_cars,
                                                                       self._event_time)
        self._simulation_core.current_number_of_cars += 1
        self._simulation_core.car_number_history.add_element_time_info(
            TimeInfo(self._event_time, self._simulation_core.current_number_of_cars))

        self._car.set_car_arrival_time(self._event_time)
        self._charger_node.connect_car(self._car)
        self.__plan_early_departure()

    def __plan_early_departure(self):
        if self._car.maximum_network_time is not None:
            maximum_departure_time = self._event_time + self._car.maximum_network_time
            latest_departure_event = LatestCarDepartureEvent(self._simulation_core, maximum_departure_time, None,
                                                             self._car)
            self._simulation_core.plan_event(latest_departure_event)
