from simulation.events.car_arrival_without_next_arrival import SimpleCarArrivalEvent


class CarArrivalEvent(SimpleCarArrivalEvent):
    def execute_action(self):
        super().execute_action()
        self._car = self._simulation_core.get_next_car()
        self._event_time = self._simulation_core.get_next_car_arrival()
        self._charger_node = self._simulation_core.get_next_charger_node()
        self._simulation_core.plan_event(self)
