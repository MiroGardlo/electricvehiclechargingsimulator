from simulation.events.CarChargingEvent import CarChargingEvent
from simulation.events.CarDepartureEvent import CarDepartureEvent


class LatestCarDepartureEvent(CarChargingEvent):
    def __init__(self, simulation_core, event_time, network_node, car):
        super().__init__(simulation_core, event_time, network_node, car, None)

    def execute_action(self):
        if self._car.is_in_network and not self._car.is_fully_charged():
            self._simulation_core.car_number_statistics.update_observation(self._simulation_core.current_number_of_cars,
                                                                           self._event_time)
            car_departure_event = CarDepartureEvent(self._simulation_core, self._event_time, None, self._car)
            self._event_detail = "Car with id: %d early arrival scheduled. Maximum time : %f" % (
                self._car.car_id, self._event_time)
            charger_node = self._car.charger_node
            if not charger_node.is_car_registered_for_departure(self._car):
                charger_node.register_car_for_departure(self._car)
                self._simulation_core.plan_event(car_departure_event)

        else:
            self._event_detail = "Car with id: %d has already left the network or is charged fully" % self._car.car_id
