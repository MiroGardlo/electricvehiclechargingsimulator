import abc


class SimulationEvent(metaclass=abc.ABCMeta):
    def __init__(self, simulation_core, event_time, event_detail=None):
        self._simulation_core = simulation_core
        self._event_time = event_time
        self._event_detail = event_detail

    @property
    def event_detail(self):
        return self._event_detail

    @property
    def event_time(self):
        return self._event_time

    @abc.abstractclassmethod
    def execute_action(self):
        pass
