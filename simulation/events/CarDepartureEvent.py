from model.strategies.chargingStrategies.budgetBasedStrategies.budget_based_user_car import BudgetBasedUserCar
from model.time_info import TimeInfo
from simulation.events.CarChargingEvent import CarChargingEvent


class CarDepartureEvent(CarChargingEvent):
    def __init__(self, simulation_core, event_time, network_node, car):
        event_detail = "Car with id: %d is leaving network at: %f" % (car.car_id, event_time)
        super().__init__(simulation_core, event_time, network_node, car, event_detail)

    def execute_action(self):
        self._simulation_core.car_number_statistics.update_observation(self._simulation_core.current_number_of_cars,
                                                                       self._event_time)
        self._simulation_core.current_number_of_cars -= 1
        self._simulation_core.car_number_history.add_element_time_info(
            TimeInfo(self._event_time, self._simulation_core.current_number_of_cars))
        self.__update_statistics()
        self.__write_car_to_file()
        self._car.disconnect_from_node()

    def __update_statistics(self):
        self.update_battery_state_observation()
        self.update_time_observation()
        if isinstance(self._car, BudgetBasedUserCar):
            self.update_budget_spending_observation()
        self._simulation_core.average_battery_state_when_leaving.add_observation(self._car.current_battery_state)
        charger_node = self._car.charger_node
        self._simulation_core.average_battery_state_level_statistics.add_observation(charger_node.node_level,
                                                                                     self._car.current_battery_state)

    def update_budget_spending_observation(self):
        budget_spent = self._car.maximum_budget - self._car.current_budget
        current_battery_state = self._car.current_battery_state
        average_paid_price_per_unit_of_energy = budget_spent / current_battery_state
        if abs(self._car.current_budget) <= 10e-8 or budget_spent == self._car.maximum_budget:
            self._simulation_core.current_number_of_cars_left_when_budget_spent += 1
        self._simulation_core.average_paid_price_per_unit_of_energy.add_observation(
            average_paid_price_per_unit_of_energy)
        self._simulation_core.average_remaining_budget.add_observation(self._car.current_budget)

    def update_time_observation(self):
        charging_time = self._event_time - self._car.car_arrival_time
        if self._car.maximum_network_time is not None:
            if abs(charging_time - self._car.maximum_network_time) < 10e-8:
                self._simulation_core.current_number_of_cars_left_when_time_elapsed += 1
                charging_time = self._car.maximum_network_time
        self._simulation_core.average_system_time_statistics.add_observation(charging_time)

    def update_battery_state_observation(self):
        if self._car.is_fully_charged():
            self._simulation_core.current_number_of_fully_served_cars += 1
        else:
            self._simulation_core.current_number_of_not_fully_served_cars += 1

    def __write_car_to_file(self):
        if not self._simulation_core.replication_mode:
            self._car.write_car_to_file()
