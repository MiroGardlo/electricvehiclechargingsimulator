from unittest import TestCase

import numpy

from electricNetwork.network import Network
from model.strategies.chargingStrategies.FlexibleUserCar import FlexibleUserCar
from model.strategies.chargingStrategies.staticUserCar import StaticUserCar
from model.strategies.experimentalStrategies.incrementalTestUser import IncrementalTestUser


class TestNetwork(TestCase):
    def setUp(self):
        self.__test_network = Network("../networks/R.csv", "../networks/X.csv", 1)
        self.__test_network.init_electric_network()

    def tearDown(self):
        self.__test_network.reset_network()

    def test_network_levels(self):
        self.assertEqual(5, self.__test_network.level_of_network)
        self.prepare_network("../networks/R2.csv", "../networks/X2.csv")
        self.assertEqual(5, self.__test_network.level_of_network)
        self.prepare_network("../networks/R3-linear10.csv", "../networks/X3-linear10.csv")
        self.assertEqual(10, self.__test_network.level_of_network)

    def test_reset(self):
        for i in range(self.__test_network.number_of_nodes - 1):
            current_node = self.__test_network.get_node(i + 1)
            car = StaticUserCar(i + 1, 0, 10, 0)
            current_node.connect_car(car)
        self.__test_network.reset_network()
        self.assertEqual(self.__test_network.max_nodal_wi, 0)
        self.assertEqual(self.__test_network.connected_cars, 0)
        for i in range(self.__test_network.number_of_nodes - 1):
            current_node = self.__test_network.get_node(i + 1)
            self.assertFalse(current_node.has_cars_to_charge())
        self.__test_network.reset_network()

    def test_number_of_nodes(self):
        self.assertEqual(self.__test_network.number_of_nodes, 6)

    def test_nodal_levels_on_bigger_network(self):
        self.prepare_network("../networks/R2.csv", "../networks/X2.csv")
        self.assertEqual(0, self.__test_network.get_node(0).node_level)

        self.assertEqual(1, self.__test_network.get_node(1).node_level)

        self.assertEqual(2, self.__test_network.get_node(2).node_level)
        self.assertEqual(2, self.__test_network.get_node(3).node_level)

        self.assertEqual(3, self.__test_network.get_node(4).node_level)
        self.assertEqual(3, self.__test_network.get_node(6).node_level)

        self.assertEqual(4, self.__test_network.get_node(8).node_level)
        self.assertEqual(4, self.__test_network.get_node(11).node_level)
        self.assertEqual(4, self.__test_network.get_node(13).node_level)
        self.assertEqual(4, self.__test_network.get_node(15).node_level)

        self.assertEqual(5, self.__test_network.get_node(17).node_level)
        self.assertEqual(5, self.__test_network.get_node(19).node_level)
        self.assertEqual(5, self.__test_network.get_node(21).node_level)
        self.assertEqual(5, self.__test_network.get_node(23).node_level)
        self.assertEqual(5, self.__test_network.get_node(25).node_level)
        self.assertEqual(5, self.__test_network.get_node(27).node_level)
        self.assertEqual(5, self.__test_network.get_node(29).node_level)
        self.assertEqual(5, self.__test_network.get_node(31).node_level)

    def prepare_network(self, r_file_matrix, x_file_matrix):
        self.__test_network = Network(r_file_matrix, x_file_matrix, 1)
        self.__test_network.init_electric_network()

    def test_update_battery_state(self):
        static_user_car = StaticUserCar(1, 0, 10, 0)
        self.__test_network.get_node(1).connect_car(static_user_car)
        self.assertEqual(static_user_car.current_battery_state, 0)
        static_user_car.update_battery_capacity(1, 1)
        self.assertEqual(static_user_car.current_battery_state, 1)
        static_user_car.update_battery_capacity(2.83, 1)
        static_user_car.update_battery_capacity(3, 1)
        static_user_car.update_battery_capacity(4, 1)
        static_user_car.update_battery_capacity(5, 1)
        static_user_car.update_battery_capacity(6.75, 1.85)
        self.assertEqual(static_user_car.current_battery_state, 6.85)
        static_user_car.update_battery_capacity(9.3, 10)
        self.assertEqual(static_user_car.current_battery_state, 10)

    def test_power_assignment_to_node_with_multiple_cars(self):
        car1 = StaticUserCar(1, 0, 10, 0)
        car2 = StaticUserCar(5, 0, 10, 0)
        car3 = StaticUserCar(4, 0, 10, 0)
        charger_node = self.__test_network.get_node(2)
        charger_node.connect_car(car1)
        charger_node.connect_car(car2)
        charger_node.connect_car(car3)

        charger_node.assign_power(10, 1, 1)

        self.assertEqual(1, car1.current_battery_state)
        self.assertEqual(5, car2.current_battery_state)
        self.assertEqual(4, car3.current_battery_state)

    def test_power_assignment_to_node_with_multiple_cars_with_willingness_to_pay_change(self):
        car1 = IncrementalTestUser(1, 0, 10, 0, 1)
        car2 = IncrementalTestUser(2, 0, 10, 0, 1)
        car3 = IncrementalTestUser(3, 0, 10, 0, 1)
        charger_node = self.__test_network.get_node(2)
        charger_node.connect_car(car1)
        charger_node.connect_car(car2)
        charger_node.connect_car(car3)

        charger_node.assign_power(6, 1, 1)

        self.assertEqual(1, car1.current_battery_state)
        self.assertEqual(2, car2.current_battery_state)
        self.assertEqual(3, car3.current_battery_state)

    def test_willingness_to_pay_statistics(self):
        car = StaticUserCar(2, 0, 10, 0)
        charger_node = self.__test_network.get_node(2)
        charger_node.connect_car(car)
        for i in range(100):
            charger_node.assign_power(0, i + 1, 1)
        self.assertEqual(car.willingness_to_pay_statistics.get_average(), 2.0)

    def test_update_wi_after_power_assignment(self):
        incremental_user = IncrementalTestUser(1, 0, 10, 0, 1)
        node = self.__test_network.get_node(1)
        node.connect_car(incremental_user)

        self.assertEqual(incremental_user.current_battery_state, 0)
        node.assign_power(1, 1, 1)
        self.assertEqual(incremental_user.willingness_to_pay, 2)
        self.assertEqual(incremental_user.current_battery_state, 1)

        node.assign_power(1, 2.83, 1)
        self.assertEqual(incremental_user.willingness_to_pay, 3)

        node.assign_power(1, 3, 1)
        self.assertEqual(incremental_user.willingness_to_pay, 4)

        node.assign_power(1, 4, 1)
        self.assertEqual(incremental_user.willingness_to_pay, 5)

        node.assign_power(1, 5, 1)
        self.assertEqual(incremental_user.willingness_to_pay, 6)

        node.assign_power(1.84, 6.75, 1)
        self.assertEqual(incremental_user.current_battery_state, 6.84)

        node.assign_power(10, 9.3, 1)
        self.assertEqual(incremental_user.current_battery_state, 10)

    def test_connected_cars(self):
        for i in range(self.__test_network.number_of_nodes):
            current_node = self.__test_network.get_node(i)
            self.assertFalse(current_node.has_cars_to_charge())

    def test_connected_cars_with_zero_willingness_to_pay(self):
        for i in range(self.__test_network.number_of_nodes):
            current_node = self.__test_network.get_node(i)
            current_node.connect_car(StaticUserCar(0, 0, 10, 0))
            current_node.connect_car(StaticUserCar(0, 0, 10, 0))
        # check if nodes with 0 nodal wi are considered to have no cars
        for i in range(self.__test_network.number_of_nodes):
            current_node = self.__test_network.get_node(i)
            self.assertFalse(current_node.has_cars_to_charge())

    def test_car_connection(self):
        test_node = self.__test_network.get_node(3)
        self.assertFalse(test_node.has_cars_to_charge())
        car = StaticUserCar(1, 0, 10, 0)
        self.assertIsNone(car.charger_node)
        test_node.connect_car(car)
        self.assertIsNotNone(car.charger_node)
        self.assertTrue(test_node.has_cars_to_charge())

    def test_static_variables_on_connection(self):
        # maximum, minimum, sum
        current_sum = 0
        min_wi = 1
        self.assertEqual(self.__test_network.connected_cars, 0)
        maximum = self.__test_network.number_of_nodes - 1
        for i in range(self.__test_network.number_of_nodes - 1):
            current_node = self.__test_network.get_node(i + 1)
            car = StaticUserCar(i + 1, 0, 10, 0)
            current_node.connect_car(car)
            current_node.assign_power(10, 0, 1)
            self.assertEqual(self.__test_network.connected_cars, i + 1)
            current_sum += i + 1
            self.assertEqual(self.__test_network.max_nodal_wi, i + 1)
            self.assertEqual(self.__test_network.nodal_wi_sum, current_sum)
        self.assertEqual(self.__test_network.connected_cars, maximum)
        for i in range(self.__test_network.number_of_nodes - 1):
            current_node = self.__test_network.get_node(i + 1)
            current_node.remove_all_cars()
            self.assertEqual(self.__test_network.connected_cars, maximum - i - 1)
            current_sum -= i + 1
            self.assertEqual(self.__test_network.nodal_wi_sum, current_sum)
        self.assertEqual(self.__test_network.connected_cars, 0)
        self.assertEqual(self.__test_network.max_nodal_wi, 0)

    def test_disconnect_car(self):
        test_node = self.__test_network.get_node(3)
        self.assertEqual(0, test_node.nodal_wi)
        car = StaticUserCar(1, 0, 10, 0)
        self.assertIsNone(car.charger_node)
        test_node.connect_car(car)
        self.assertEqual(1, test_node.nodal_wi)
        self.assertIsNotNone(car.charger_node)
        test_node.register_car_for_departure(car)
        car.disconnect_from_node()
        self.assertEqual(0, test_node.nodal_wi)
        self.assertFalse(test_node.has_cars_to_charge())
        self.assertIsNone(car.charger_node)

    def test_early_departure(self):
        car = StaticUserCar(1, 0, 10, 0)
        self.assertFalse(car.is_ready_to_leave_network(100))
        car.update_battery_capacity(10, 10)
        self.assertTrue(car.is_ready_to_leave_network(100))

        test_node = self.__test_network.get_node(2)
        self.assertEqual(0, test_node.nodal_wi)
        car2 = FlexibleUserCar(1, 0, 10, 0, 200, 1)
        car2.connect_to_node(test_node)
        self.assertFalse(car2.is_ready_to_leave_network(100))
        car2.update_battery_capacity(199, 9)
        self.assertTrue(car.is_ready_to_leave_network(200))
        car2.update_battery_capacity(200, 10)
        self.assertTrue(car.is_ready_to_leave_network(200))
        self.assertTrue(car.is_fully_charged())

    def test_nodal_wi(self):
        test_node = self.__test_network.get_node(3)
        self.assertFalse(test_node.has_cars_to_charge())
        self.assertEqual(0, test_node.nodal_wi)
        test_node.connect_car(StaticUserCar(1, 0, 10, 0))
        self.assertEqual(1, test_node.nodal_wi)
        test_node.connect_car(StaticUserCar(1, 0, 10, 0))
        self.assertEqual(2, test_node.nodal_wi)

    def test_electric_network_structure(self):
        root = self.__test_network.get_node(0)
        self.assertEqual(root.node_number, 0)
        self.assertEqual(root.node_degree, 1)
        # node number 5 has only one neighbour
        last_node = self.__test_network.get_node(5)
        self.assertEqual(last_node.node_number, 5)
        self.assertEqual(last_node.node_degree, 1)
        # test all other nodes
        for i in range(1, self.__test_network.number_of_nodes - 1, 1):
            network_node = self.__test_network.get_node(i)
            self.assertEqual(network_node.node_number, i)
            # each node has two neighbours except root
            self.assertEqual(network_node.node_degree, 2)

    def test_edge_dictionary(self):
        number_of_edges = 0
        for edge in self.__test_network:
            self.assertEqual(edge[0], number_of_edges)
            number_of_edges += 1
        self.assertEqual(5, number_of_edges)

    def test_edge_number(self):
        self.test_number_of_edges()
        test_node = self.__test_network.get_node(1)
        self.assertEqual(test_node.node_degree, 2)
        self.assertEqual(test_node.get_neighbour(0).edge_number, 0)
        self.assertEqual(test_node.get_neighbour(1).edge_number, 1)
        test_node2 = self.__test_network.get_node(4)
        self.assertEqual(test_node2.node_degree, 2)
        self.assertEqual(test_node2.get_neighbour(0).edge_number, 3)
        self.assertEqual(test_node2.get_neighbour(1).edge_number, 4)

    def test_edge_number_of_neighbour(self):
        self.test_number_of_edges()
        test_node = self.__test_network.get_node(3)
        self.assertEqual(test_node.edge_number_of_neighbour(4), 3)

    def test_number_of_edges(self):
        self.assertEqual(self.__test_network.number_of_edges, 5)
