from unittest import TestCase

from simulation.events.event import SimulationEvent
from simulation.simulationCore.eventSimulationCore import EventSimulationCore


class EventSimulationCoreTest(TestCase):
    def test_event_planning(self):
        core = FakeEventSimulationCore()
        core.plan_event(FakeSimulationEvent(core, 20))
        core.plan_event(FakeSimulationEvent(core, 10))
        core.plan_event(FakeSimulationEvent(core, 0))
        core.plan_event(FakeSimulationEvent(core, 0))
        time_line = core.time_line
        self.assertEqual(time_line.queue.__len__(), 4)
        self.assertEqual(time_line.get()[0], 0)
        self.assertEqual(time_line.get()[0], 0)
        self.assertEqual(time_line.get()[0], 10)
        self.assertEqual(time_line.get()[0], 20)

    def test_failed_event_planning(self):
        core = FakeEventSimulationCore()
        core.plan_event(FakeSimulationEvent(core, 10))
        core.plan_event(FakeSimulationEvent(core, 20))
        self.assertRaises(ValueError, core.plan_event, FakeSimulationEvent(core, -1))


class FakeSimulationEvent(SimulationEvent):
    def __init__(self, simulation_core, event_time):
        super().__init__(simulation_core, event_time)

    def execute_action(self):
        pass


class FakeEventSimulationCore(EventSimulationCore):
    @property
    def time_line(self):
        return self._event_queue

    def before_simulation_start(self):
        pass

    def before_replication_start(self):
        pass

    def on_simulation_end(self):
        pass

    def on_replication_end(self):
        pass
