from unittest import TestCase

from simulation.simulationUtils.generators.EmpiricRNG import EmpiricRNG
from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class EmpiricDistributionTest(TestCase):
    def test_generator_creation(self):
        empiric_distribution = EmpiricRNG([UniformRNG(0, 1), 0.7], [UniformRNG(1000, 50000), 0.3])
        sample = empiric_distribution.generate_sample()
        self.assertTrue(0 <= sample < 50000)

    def test_generator_creation_with_sum_numerically_close_to_one(self):
        empiric_distribution = EmpiricRNG([UniformRNG(0, 1), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1],
                                          [UniformRNG(1000, 50000), 0.1])
        sample = empiric_distribution.generate_sample()
        self.assertTrue(0 <= sample < 50000)

    def test_creation_exception(self):
        self.assertRaises(ValueError, EmpiricRNG, [UniformRNG(0, 1), 0.7], [UniformRNG(1000, 50000), 0.3],
                          [UniformRNG(1000, 50000), 0.9])

    def test_generator_size(self):
        empiric_distribution = EmpiricRNG([UniformRNG(0, 1), 0.2], [UniformRNG(1000, 50000), 0.2],
                                          [UniformRNG(1000, 50000), 0.6])

        self.assertEqual(3, empiric_distribution.generator_size)
