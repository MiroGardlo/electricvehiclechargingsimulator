from unittest import TestCase

from simulation.simulationUtils.generators.uniformDiscreteRNG import UniformDiscreteRNG
from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class UniformRNGTest(TestCase):
    def test_uniform_continuous_values(self):
        uniform_rng = UniformRNG(-10, 10)
        for i in range(10000000):
            self.assertTrue(-10 <= uniform_rng.generate_sample() < 10)

    def test_uniform_discrete_values(self):
        uniform_rng = UniformDiscreteRNG(-10, 10)
        for i in range(10000000):
            sample = uniform_rng.generate_sample()
            self.assertTrue(isinstance(sample, int))
            self.assertTrue(-10 <= sample < 10)
