from unittest import TestCase

from model.strategies.chargingStrategies.budgetBasedStrategies.constant_energy_gainer_user import \
    ConstantEnergyGainerUser
from model.strategies.chargingStrategies.budgetBasedStrategies.constant_per_time_spender import ConstantPerTimeSpender
from model.strategies.chargingStrategies.staticUserCar import StaticUserCar


class StrategiesTest(TestCase):
    def test_should_leave_network(self):
        static_user_car = StaticUserCar(1, 0, 10, 0)
        self.assertFalse(static_user_car.assigned_power_exceeds_max_capacity(3))
        self.assertTrue(static_user_car.assigned_power_exceeds_max_capacity(10))
        static_user_car.update_battery_capacity(10, 100)
        self.assertEqual(10, static_user_car.current_battery_state)
        self.assertTrue(static_user_car.is_ready_to_leave_network(10))

    def test_car_creation(self):
        car1 = StaticUserCar(1, 0, 10, 0)
        self.assertIsNone(car1.charger_node)

        car2 = ConstantEnergyGainerUser(1, 0, 20, 0, 1000, 500, 0.5, 1)
        self.assertIsNotNone(car2.decision_interval)

        car3 = ConstantPerTimeSpender(1, 0, 20, 0, 1000)
        self.assertIsNone(car3.decision_interval)
