from unittest import TestCase

from simulation.simulationUtils.level_based_statistics import LevelBasedStatistics
from simulation.simulationUtils.replication_level_based_statistics import ReplicationLevelBasedStatistics
from simulation.simulationUtils.statistics import Statistics


class LevelStatisticsTest(TestCase):
    def test_level_based_statistics(self):
        number_of_levels = 5
        statistics = LevelBasedStatistics(number_of_levels)
        replication_statistics = ReplicationLevelBasedStatistics(number_of_levels)

        self.__assert_empty(statistics, number_of_levels)
        self.__fill_statistics(statistics, replication_statistics)
        self.__assert_filled(statistics, number_of_levels)

        self.__assert_statistics(statistics)
        self.__assert_statistics(replication_statistics)
        self.__assert_clear(statistics)

    def test_update_statistics_from_statistics(self):
        statistics = Statistics()
        statistics.add_observation(1)
        statistics.add_observation(2)
        statistics.add_observation(3)

        statistics2 = Statistics()
        statistics2.add_observation(3)
        statistics2.add_observation(4)
        statistics2.add_observation(5)
        statistics2.update_observation_from_statistics(statistics)
        self.assertEqual(statistics2.get_mean(), 3)
        self.assertTrue(abs(statistics2.get_standard_deviation() - 1.290994449) < 1e-9)
        self.assertTrue(abs(statistics2.get_standard_error() - 0.52704627669) < 1e-9)

    def test_update_from_empty(self):
        statistics = Statistics()
        statistics2 = Statistics()
        statistics3 = Statistics()
        statistics4 = Statistics()
        statistics_global = Statistics()

        statistics4.add_observation(3)

        statistics_global.update_observation_from_statistics(statistics)
        statistics_global.update_observation_from_statistics(statistics2)
        statistics_global.update_observation_from_statistics(statistics3)

        self.assertEqual(statistics_global.get_mean(), 0)
        self.assertEqual(statistics_global.get_standard_deviation(), -1)
        self.assertEqual(statistics_global.get_standard_error(), -1)

        statistics_global.update_observation_from_statistics(statistics4)

        self.assertEqual(statistics_global.get_mean(), 3)
        self.assertEqual(statistics_global.get_standard_deviation(), 0)
        self.assertEqual(statistics_global.get_standard_error(), 0)

    def __assert_statistics(self, statistics):
        self.assertEqual(1, statistics.get_mean(1))
        self.assertEqual(2, statistics.get_mean(2))
        self.assertEqual(3, statistics.get_mean(3))
        self.assertEqual(4, statistics.get_mean(4))
        self.assertEqual(5, statistics.get_mean(5))

    @staticmethod
    def __fill_statistics(statistics, replication_statistics):
        statistics.add_observation(1, 1)
        statistics.add_observation(2, 1)
        statistics.add_observation(3, 1)
        statistics.add_observation(4, 1)
        statistics.add_observation(5, 1)
        statistics.add_observation(1, 1)
        statistics.add_observation(2, 3)
        statistics.add_observation(3, 5)
        statistics.add_observation(4, 7)
        statistics.add_observation(5, 9)
        replication_statistics.add_observation(statistics)

    def __assert_clear(self, statistics):
        statistics.clear()
        self.assertEqual(0, statistics.get_mean(1))
        self.assertEqual(0, statistics.get_mean(2))
        self.assertEqual(0, statistics.get_mean(3))
        self.assertEqual(0, statistics.get_mean(4))
        self.assertEqual(0, statistics.get_mean(5))

    def __assert_empty(self, statistics, number_of_levels):
        for i in range(number_of_levels):
            self.assertFalse(statistics.get_level_statistics(i + 1).has_observation)

    def __assert_filled(self, statistics, number_of_levels):
        for i in range(number_of_levels):
            self.assertTrue(statistics.get_level_statistics(i + 1).has_observation)
