from queue import PriorityQueue
from unittest import TestCase

from simulation.simulationUtils.generators.uniformRNG import UniformRNG


class PriorityQueueTest(TestCase):
    def test_order(self):
        random_generator = UniformRNG(0, 100000)
        number_of_elements = 100000
        queue = PriorityQueue()
        for i in range(number_of_elements):
            queue.put(random_generator.generate_sample())
        previous_element = 0
        for i in range(100000):
            current_element = queue.get()
            self.assertTrue(previous_element <= current_element)
            previous_element = current_element

    def test_minimum_first(self):
        random_generator = UniformRNG(0, 100000)
        for i in range(10000):
            number_of_elements = 1000
            queue = PriorityQueue()
            for j in range(number_of_elements):
                queue.put(random_generator.generate_sample())
            self.assertTrue(min(queue.queue), queue.queue[0])
