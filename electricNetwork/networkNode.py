from electricNetwork.nodalNeighbour import NodalNeighbour


class NetworkNode:
    def __init__(self, node_number, network):
        """
        Creates new network node with specified id number. Node does not contain
        any connected cars or neighbours
        :param node_number:
        """
        self.__network = network
        self.__node_number = node_number
        # no cars initially connected to the node
        self._connected_cars = {}
        self.__node_neighbours = []
        self.__cars_ready_to_leave = {}
        self.__nodal_willingness_to_pay = 0
        self.__node_level = 0

    @property
    def node_level(self):
        return self.__node_level

    @node_level.setter
    def node_level(self, value):
        self.__node_level = value

    @property
    def node_number(self):
        return self.__node_number

    @property
    def node_degree(self):
        return self.__node_neighbours.__len__()

    # Nodal willingness to pay is computed as a sum of w_i of customers connected
    # to this node change this in the future
    @property
    def nodal_wi(self) -> float:
        """
        Returns nodal willingness to pay computed as a sum of wi parameters of
        connected cars
        :return: nodal_wi
        """
        return self.__nodal_willingness_to_pay

    def update_nodal_wi(self, previous_value, new_value):
        """
        Updates nodal willingness to pay. This method has to be called when any of the connected cars
        updates its willingness to pay.
        :param previous_value: previous willingness to pay value
        :param new_value: new willingness to pay value
        :return:
        """
        self.__nodal_willingness_to_pay = self.__nodal_willingness_to_pay - previous_value + new_value
        self.__check_willingness_to_pay()
        self.__network.add_nodal_wi_sum(new_value - previous_value)

    def get_neighbour(self, index) -> NodalNeighbour:
        """
        Returns neighbour of this node with specified index
        :param index:
        :return: returns specified neighbour of this node
        """
        return self.__node_neighbours[index]

    def has_cars_to_charge(self) -> bool:
        """
        Checks whether this node has cars attached to this node. If there are cars connected
        method checks whether nodal willingness to pay is greater than zero.
        :return: returns true if there is at least one car connected to this node and nodal willingness
        to pay is greater than zero false otherwise
        """
        return self._connected_cars.__len__() > 0 and self.__nodal_willingness_to_pay > 0

    def has_cars_to_disconnect(self) -> bool:
        """
        Checks whether this node has any cars that are ready to leave the network
        :return: returns true if there is at least one car connected to this node that is supposed to leave
        the network,false otherwise
        """
        return self.__cars_ready_to_leave.__len__() > 0

    def connect_car(self, car):
        """
        Connects specified car to this node
        :param car: car to be connected to this node
        :return: None
        """
        self._connected_cars[car.car_id] = car
        self.__nodal_willingness_to_pay += car.willingness_to_pay
        car.connect_to_node(self)
        self.__network.add_nodal_wi_sum(car.willingness_to_pay)
        self.__network.car_connected()

    def assign_power(self, real_power, simulation_time, time_step):
        """
        Power is assigned to this node is divided among cars connected to this node
        :param real_power: power assigned to this node
        :param simulation_time: current simulation time
        :param time_step: time step that simulation core performed
        :return: None
        """
        if self.has_cars():
            self.__update_car_battery_capacity(real_power, simulation_time, time_step)
            self.__update_willingness_to_pay_of_connected_cars(simulation_time, time_step)

    def __update_car_battery_capacity(self, real_power, simulation_time, time_step):
        for car in self._connected_cars.values():
            car_power = 0
            if self.nodal_wi > 0:
                car_power = real_power * car.willingness_to_pay / self.nodal_wi
                if car_power < 0:
                    raise ValueError("Can't assign negative power!")
            car.update_battery_capacity(simulation_time, car_power * time_step)
            if car.is_ready_to_leave_network(simulation_time):
                self.register_car_for_departure(car)

    def register_car_for_departure(self, car):
        self.__car_ready_to_leave(car)
        self.__network.car_ready_to_leave()

    def is_car_registered_for_departure(self, car):
        return car.car_id in self.__cars_ready_to_leave

    def disconnect_car(self, car_id):
        """
        Disconnects specified car from this node
        :param car_id: car to be disconnected from the node
        :return: None
        """
        car = self._connected_cars[car_id]
        self.update_nodal_wi(car.willingness_to_pay, 0)
        self.__update_network_information()
        del self.__cars_ready_to_leave[car_id]
        del self._connected_cars[car_id]

    def disconnect_all_ready_vehicles(self):
        for car in self.__cars_ready_to_leave.values():
            car.disconnect_from_node()
        self.__cars_ready_to_leave.clear()

    def cars_to_leave_network(self):
        return list(self.__cars_ready_to_leave.values())

    def remove_all_cars(self):
        """
        Method used to remove all cars from this node
        :return: None
        """
        connected_cars = self._connected_cars.__len__()
        cars_ready_to_leave = self.__cars_ready_to_leave.__len__()
        self._connected_cars.clear()
        self.__cars_ready_to_leave.clear()
        self.__network.add_nodal_wi_sum(-self.__nodal_willingness_to_pay)
        self.__nodal_willingness_to_pay = 0
        self.__network.cars_removed(connected_cars)
        self.__network.ready_cars_left(cars_ready_to_leave)

    def add_neighbour(self, neighbouring_node, edge_properties):
        """
        Adds neighbour of this node
        :param neighbouring_node: node to add as a neighbour
        :param edge_properties: properties specifying parameters of the
         edge composed of this node and the new neighbour
        :return:
        """
        self.__node_neighbours.append(NodalNeighbour(neighbouring_node, edge_properties))

    def edge_number_of_neighbour(self, neighbour_id):
        """
        Returns edge number of edge with composed from this node and node with specified id
        :param neighbour_id: id of neighbour
        :return: edge number of neighbour
        """
        neighbour = next(x for x in self.__node_neighbours if x.node.node_number == neighbour_id)
        return neighbour.edge_properties.edge_number

    def __car_ready_to_leave(self, car):
        self.__cars_ready_to_leave[car.car_id] = car

    def __update_network_information(self):
        self.__network.ready_car_left()
        self.__network.car_removed()

    def has_cars(self):
        return self._connected_cars.__len__() > 0

    def print_all_cars_to_file(self):
        for car in self._connected_cars.values():
            car.write_car_to_file()

    def __update_willingness_to_pay_of_connected_cars(self, simulation_time, current_time_change):
        for car in self._connected_cars.values():
            car.trigger_willingness_to_pay_update(simulation_time, current_time_change)

    def __check_willingness_to_pay(self):
        if abs(self.__nodal_willingness_to_pay) < 10e-8:
            self.__nodal_willingness_to_pay = 0
