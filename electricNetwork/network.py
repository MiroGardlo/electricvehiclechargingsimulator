import numpy

import electricNetwork.networkNode
from electricNetwork.edgeProperties import EdgeProperties
from electricNetwork.graphEdge import GraphEdge


class Network:
    def __init__(self, resistance_file, reactance_file, norm_voltage):
        self.__nodal_wi_sum = 0
        self.__number_of_cars_to_leave = 0
        self.__number_of_connected_cars = 0
        self.__resistance_file = resistance_file
        self.__reactance_file = reactance_file
        self._graph_nodes = []
        self.__graph_edges = {}
        self.__norm_voltage = norm_voltage
        self.__number_of_edges = 0
        self.__number_of_levels = 0

    @property
    def nodal_wi_sum(self):
        return self.__nodal_wi_sum

    @property
    def max_nodal_wi(self):
        max_wi = -numpy.inf
        for i in range(1, self.number_of_nodes):
            current_node = self.get_node(i)
            if current_node.nodal_wi > max_wi:
                max_wi = current_node.nodal_wi
        return max_wi

    @property
    def number_of_nodes(self):
        return len(self._graph_nodes)

    @property
    def norm_voltage(self):
        return self.__norm_voltage

    @property
    def connected_cars(self):
        return self.__number_of_connected_cars

    @property
    def cars_to_leave(self):
        return self.__number_of_cars_to_leave

    @property
    def level_of_network(self):
        return self.__number_of_levels

    def __iter__(self):
        """
        Iterator that iterates over graph edges
        :return: iterator
        """
        return iter(self.__graph_edges.items())

    @property
    def number_of_edges(self):
        return self.__number_of_edges

    def add_nodal_wi_sum(self, value_to_add):
        self.__nodal_wi_sum += value_to_add
        if abs(self.__nodal_wi_sum) < 10e-8:
            self.__nodal_wi_sum = 0

    def car_connected(self):
        self.__number_of_connected_cars += 1

    def car_removed(self):
        self.__number_of_connected_cars -= 1

    def cars_removed(self, number_of_cars):
        self.__number_of_connected_cars -= number_of_cars

    def car_ready_to_leave(self):
        self.__number_of_cars_to_leave += 1

    def ready_car_left(self):
        self.__number_of_cars_to_leave -= 1

    def ready_cars_left(self, number_of_cars):
        self.__number_of_cars_to_leave -= number_of_cars

    def get_edge(self, edge_number) -> GraphEdge:
        """
        Method returns edge with specified edge number
        :param edge_number: identifier of edge to return
        :return: edge with specified edge number
        """
        return self.__graph_edges[edge_number]

    def get_node(self, index) -> electricNetwork.networkNode.NetworkNode:
        """
        Method returns node of electric network that with specified number.
        If node doesn't exist none is returned
        :param index: node number
        :return: node with specified number
        """
        if len(self._graph_nodes) > index >= 0:
            return self._graph_nodes[index]

    def init_electric_network(self):
        """
            Initializes structure of electric network and sets reactance, resistance, conductance and
            susceptance parameters of edges in the network
        :return: none
        """
        resistance_matrix = numpy.loadtxt(self.__resistance_file, delimiter=',')
        reactance_matrix = numpy.loadtxt(self.__reactance_file, delimiter=',')
        self.__create_graph_structure(resistance_matrix, reactance_matrix)

    def __create_graph_structure(self, resistance_matrix, reactance_matrix):
        rows = resistance_matrix.shape[0]
        columns = resistance_matrix.shape[1]
        self.__init_graph_nodes(columns)
        # only check upper triangle since matrix is symmetrical
        for i in range(rows):
            for j in range(i, columns):
                if resistance_matrix[i][j] != 0:
                    edge_info = EdgeProperties(resistance_matrix[i][j], reactance_matrix[i][j], self.__number_of_edges)
                    starting_node = self._graph_nodes[i]
                    end_node = self._graph_nodes[j]
                    starting_node.add_neighbour(end_node, edge_info)
                    end_node.add_neighbour(starting_node, edge_info)
                    end_node.node_level = starting_node.node_level + 1
                    self.__graph_edges[self.number_of_edges] = {GraphEdge(starting_node, end_node, edge_info)}
                    self.__number_of_edges += 1
                    if end_node.node_level > self.__number_of_levels:
                        self.__number_of_levels = end_node.node_level

    def __init_graph_nodes(self, number_of_nodes):
        for i in range(number_of_nodes):
            self._graph_nodes.append(electricNetwork.networkNode.NetworkNode(i, self))

    def reset_network(self):
        for i in range(1, self.number_of_nodes):
            current_node = self._graph_nodes[i]
            current_node.remove_all_cars()
        self.__number_of_connected_cars = 0
        self.__number_of_cars_to_leave = 0
        self.__nodal_wi_sum = 0

    def print_all_vehicles(self):
        for i in range(1, self.number_of_nodes):
            current_node = self._graph_nodes[i]
            if current_node.has_cars():
                current_node.print_all_cars_to_file()
