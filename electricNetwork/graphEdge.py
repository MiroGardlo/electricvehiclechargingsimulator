class GraphEdge:
    # resistance - r parameter, reactance - x parameter, g - conductance, b - susceptance
    def __init__(self, node1, node2, edge_properties):
        """
        Creates new edge between specified nodes
        :param node1: first node of the edge
        :param node2: second node of the edge
        :param edge_properties: properties of the edge
        """
        self.__node1 = node1
        self.__node2 = node2
        self.__edge_properties = edge_properties

    @property
    def reactance(self):
        return self.__edge_properties.reactance

    @property
    def conductance(self):
        return self.__edge_properties.conductance

    @property
    def starting_node(self):
        return self.__node1

    @property
    def end_node(self):
        return self.__node2

    @property
    def edge_number(self):
        return self.__edge_properties.edge_number
