class NodalNeighbour:
    def __init__(self, node, edge_properties):
        """
        Class represents a neighbour of node.
        :param node: end node
        :param edge_properties: edge properties
        """
        self.__node = node
        self.__edge_properties = edge_properties

    @property
    def node(self):
        return self.__node

    @property
    def edge_number(self):
        return self.__edge_properties.edge_number

    @property
    def edge_properties(self):
        return self.__edge_properties

