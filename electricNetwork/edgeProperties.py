class EdgeProperties:
    """
        Class represents information about specific edge in the graph structure
    """

    # resistance - r parameter, reactance - x parameter, g - conductance, b - susceptance
    def __init__(self, resistance, reactance, edge_number):
        """
        Creates new edge information
        :param resistance: resistance of the edge
        :param reactance: reactance of the edge
        :param edge_number: edge number
        """
        self.__edge_number = edge_number
        self.__resistance = resistance
        self.__reactance = reactance
        self.__conductance = self.__compute_conductance(resistance, reactance)
        self.__susceptance = self.__compute_susceptance(resistance, reactance)

    @property
    def resistance(self):
        return self.__resistance

    @property
    def reactance(self):
        return self.__reactance

    @property
    def conductance(self):
        return self.__conductance

    @property
    def susceptance(self):
        return self.__susceptance

    @property
    def edge_number(self):
        return self.__edge_number

    @staticmethod
    def __compute_susceptance(resistance, reactance):
        return reactance / (resistance ** 2 + reactance ** 2)

    @staticmethod
    def __compute_conductance(resistance, reactance):
        return resistance / (resistance ** 2 + reactance ** 2)
